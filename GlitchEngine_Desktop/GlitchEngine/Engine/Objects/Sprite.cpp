#include "Sprite.h"

Sprite::Sprite()
{
}

Sprite::Sprite(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, bool translucent, string image)
{
	material = new Material("GE_Sprited", translucent);
	material->SetTexture("texture", image);
	material->SetVec2("texel", Vector<glm::vec2>(1, 1));
	material->SetVec4("frame", Vector<glm::vec4>(0, 0, 1, 1));

	transform->Translate(position, Space::WorldSpace);
	transform->Rotate(rotation);
	transform->ScaleObject(size);
	transform->SetLayer(sLayer);

	if (material != nullptr)
	{
		int arrayPos = material->GetArrayPosition();
		if (arrayPos != -1)
		{
			AddToRendererList(arrayPos);
			material->AddParentPosition(positionInGroup);
		}
		else
		{
			Logger::Log("Quad Error!: Make sure to set Material textures before initializing.");
			delete this;
		}
	}
	else
	{
		Logger::Log("Quad Error!: You are passing a null Material value.");
		delete this;
	}
}

Sprite::~Sprite()
{
}

void Sprite::SetTexelSize(Vector<glm::vec2> size)
{
	material->SetVec2("texel", size);
}

void Sprite::SetFrame(Vector<glm::vec4> frame)
{
	material->SetVec4("frame", frame);
}
