﻿#include <Objects/Quad.h>
#include <Classes/Renderer.h>

Quad::Quad()
{
}

Quad::Quad(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, Material* mat)
{
	material = mat;
	transform->Translate(position, Space::WorldSpace);
	transform->Rotate(rotation);
	transform->ScaleObject(size);
	transform->SetLayer(sLayer);

	if (material != nullptr)
	{
		int arrayPos = material->GetArrayPosition();
		if (arrayPos != -1)
		{
			AddToRendererList(arrayPos);
			material->AddParentPosition(positionInGroup);
		}
		else
		{
			Logger::Log("Quad Error!: Make sure to set Material textures before initializing.");
			delete this;
		}
	}
	else
	{
		Logger::Log("Quad Error!: You are passing a null Material value.");
		delete this;
	}
}

Quad::~Quad()
{
	if (material != nullptr)
	{
		Renderer::AvailableSpaces[material->GetArrayPosition()].push_back(positionInGroup);
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[material->GetArrayPosition()]["Model"]);
		glBufferSubData(GL_ARRAY_BUFFER, positionInGroup * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(glm::mat4(0.f)));
	}
}


Material* Quad::GetMaterial() const
{
	return material;
}

void Quad::SetMaterial(Material* mat)
{
	material = mat;
}

void Quad::SendModel()
{
	if (isVisible)
	{
		glm::mat4 Model = transform->GetMatrix();
		Vector<glm::vec2> scaleFactor = (&ApplicationData::GetInstance())->aspectScale;
		Model = glm::scale(Model, glm::vec3(scaleFactor.Get<0>(), scaleFactor.Get<1>(), 1.f));
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[material->GetArrayPosition()]["Model"]);
		glBufferSubData(GL_ARRAY_BUFFER, positionInGroup * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(Model));
		transform->ResetUpdateStatus();
	}
	else
	{
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[material->GetArrayPosition()]["Model"]);
		glBufferSubData(GL_ARRAY_BUFFER, positionInGroup * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(glm::mat4(0.f)));
		transform->ResetUpdateStatus();
	}
}

void Quad::SetSortingLayer(short layer)
{
	transform->SetLayer(layer);
}

short Quad::GetSortingLayer() const
{
	return transform->GetLayer();
}

void Quad::SetVisibility(bool visible)
{
	isVisible = visible;
}

bool Quad::GetVisibility() const
{
	return isVisible;
}

void Quad::AddToRendererList(GLuint arrayPosition)
{
	if (Renderer::AvailableSpaces[arrayPosition].size() > 0)
	{
		positionInGroup = Renderer::AvailableSpaces[arrayPosition][0];
		Renderer::AvailableSpaces[arrayPosition].erase(Renderer::AvailableSpaces[arrayPosition].begin());
	}
	else
	{
		positionInGroup = Renderer::RenderableObjects[arrayPosition];
		Renderer::RenderableObjects[arrayPosition]++;
	}
}