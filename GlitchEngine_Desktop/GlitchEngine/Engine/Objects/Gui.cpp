#include <Objects/Gui.h>

Gui &Gui::GetInstance() {

	static Gui *gui = NULL;

	if (gui == NULL)
		gui = new Gui();

	return *gui;
}

Gui::Gui()
{
	appData = &ApplicationData::GetInstance();
	glm::vec2 screenSize = glm::vec2(appData->renderWidth, appData->renderHeight);
	guiMat = glm::ortho(0.f, screenSize.x, screenSize.y, 0.f, -1.f, 1000.f);

	RM = &ResourceManager::GetInstance();
	input = &Input::GetInstance();

	fontColor = glm::vec3(1, 1, 1);

	fontShaderID = ShaderLoader::GetProgramIDAt("GE_Font");
	buttonShaderID = ShaderLoader::GetProgramIDAt("GE_Botton");

	glGenVertexArrays(1, &guiVertexArrayID);
	glBindVertexArray(guiVertexArrayID);

	glGenBuffers(1, &fontVBO);
	glGenBuffers(1, &fontUV);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, fontVBO);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, fontUV);
	glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

	uniformLocations[fontShaderID]["MVP"] = glGetUniformLocation(fontShaderID, "MVP");
	uniformLocations[fontShaderID]["Color"] = glGetUniformLocation(fontShaderID, "Color");
	uniformLocations[fontShaderID]["texture"] = glGetUniformLocation(fontShaderID, "texture");

	uniformLocations[buttonShaderID]["MVP"] = glGetUniformLocation(buttonShaderID, "MVP");
	uniformLocations[buttonShaderID]["texture"] = glGetUniformLocation(buttonShaderID, "texture");

	glUseProgram(fontShaderID);
	glUniformMatrix4fv(uniformLocations[fontShaderID]["MVP"], 1, GL_FALSE, glm::value_ptr(guiMat));
	glUniform3f(uniformLocations[fontShaderID]["Color"], fontColor.x, fontColor.y, fontColor.z);
	glUseProgram(0);

	glUseProgram(buttonShaderID);
	glUniformMatrix4fv(uniformLocations[buttonShaderID]["MVP"], 1, GL_FALSE, glm::value_ptr(guiMat));
	glUseProgram(0);
	glBindVertexArray(0);
}

void Gui::Destroy()
{
	Gui *gui = &GetInstance();
	if (gui != NULL)
		delete gui;
}

Gui::~Gui()
{
	glDeleteBuffers(1, &fontUV);
	glDeleteBuffers(1, &fontVBO);
	glDeleteVertexArrays(1, &guiVertexArrayID);

	map<FontName, FontAtlas*>::iterator it = fonts.begin();
	for (it; it != fonts.end(); it++)
		delete it->second->mat;
}

void Gui::SetVertexArray()
{
	glBindVertexArray(guiVertexArrayID);
}

void Gui::Label(const char* text, Vector<glm::vec2> coords, string fontName, int fontSize)
{
	FontName name = make_pair(fontName, fontSize);
	map<FontName, FontAtlas*>::iterator it = fonts.find(name);
	if (it == fonts.end())
		fonts[name] = new FontAtlas(RM->faces[fontName], fontSize, fontName);

	FontAtlas* currentAtlas = fonts[name];

	quadVerts.clear();
	quadUVs.clear();

	glUseProgram(fontShaderID);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, currentAtlas->mat->GetFontTextureID());
	glUniform1i(uniformLocations[fontShaderID]["texture"], 0);

	float penX = coords.Get<0>();
	float penY = coords.Get<1>() + currentAtlas->textH;
	for (const char *p = text; *p; p++)
	{
		char currentChar = *p - 32;

		float x = penX + currentAtlas->chars[currentChar].bLeft;
		float y = penY - currentAtlas->chars[currentChar].bTop;
		unsigned int w = currentAtlas->chars[currentChar].bWidth;
		unsigned int h = currentAtlas->chars[currentChar].bHeight;

		float tx = currentAtlas->chars[currentChar].tOffSetX;
		float tw = currentAtlas->chars[currentChar].tOffSetX + currentAtlas->chars[currentChar].tOffSetXAdvance;
		float ty = currentAtlas->chars[currentChar].tOffSetYAdvance;

		quadVerts.push_back(x);
		quadVerts.push_back(y);
		quadVerts.push_back(0.f);

		quadVerts.push_back(x);
		quadVerts.push_back(y + h);
		quadVerts.push_back(0.f);

		quadVerts.push_back(x + w);
		quadVerts.push_back(y + h);
		quadVerts.push_back(0.f);

		quadVerts.push_back(x + w);
		quadVerts.push_back(y);
		quadVerts.push_back(0.f);

		quadUVs.push_back(tx);
		quadUVs.push_back(0.f);

		quadUVs.push_back(tx);
		quadUVs.push_back(ty);

		quadUVs.push_back(tw);
		quadUVs.push_back(ty);

		quadUVs.push_back(tw);
		quadUVs.push_back(0.f);

		penX += currentAtlas->chars[currentChar].advanceX;
		penY += currentAtlas->chars[currentChar].advanceY;
	}

	glBindBuffer(GL_ARRAY_BUFFER, fontVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadVerts.size(), &quadVerts[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, fontUV);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadUVs.size(), &quadUVs[0], GL_STATIC_DRAW);

	glDrawArrays(GL_QUADS, 0, quadVerts.size() / 3);
}

void Gui::SetFontColor(Vector<glm::vec3> color)
{
	fontColor = color.GetVec();
	glUseProgram(fontShaderID);
	glUniform3f(uniformLocations[fontShaderID]["Color"], fontColor.x, fontColor.y, fontColor.z);
}

void Gui::Button(Vector<glm::vec2> coords, Vector<glm::vec2> size, string textureN)
{
	glUseProgram(buttonShaderID);
	TextureInfo text = RM->textures[textureN];

	quadVerts.clear();
	quadUVs.clear();

	quadVerts = {
		coords.Get<0>(), coords.Get<1>(), 0.f,
		coords.Get<0>(), coords.Get<1>() + size.Get<1>(), 0.f,
		coords.Get<0>() + size.Get<0>(), coords.Get<1>() + size.Get<1>(), 0.f,
		coords.Get<0>() + size.Get<0>(), coords.Get<1>(), 0.f
	};

	quadUVs = {
		0.f, 0.f,
		0.f, 1.f,
		1.f, 1.f,
		1.f, 0.f
	};

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, text.bufferID);

	glBindBuffer(GL_ARRAY_BUFFER, fontVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadVerts.size(), &quadVerts[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, fontUV);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadUVs.size(), &quadUVs[0], GL_STATIC_DRAW);

	glDrawArrays(GL_QUADS, 0, quadVerts.size() / 3);
	glUseProgram(0);

	glm::vec2 cursorPos = input->GetScreenCursorPosition().GetVec();
	coords = coords + Vector<glm::vec2>(appData->baseSize.z, appData->baseSize.w);

	if (cursorPos.x >= coords.Get<0>() && cursorPos.x <= coords.Get<0>() + size.Get<0>() && cursorPos.y >= coords.Get<1>() && cursorPos.y <= coords.Get<1>() + size.Get<1>())
	{
		if (input->GetMouseButtonUp(SDL_BUTTON_LEFT))
			(&LuaEventDispatcher::GetInstance())->TriggerLuaEvent();
	}
}

void Gui::Image(Vector<glm::vec2> coords, Vector<glm::vec2> size, string textureN)
{
	glUseProgram(buttonShaderID);
	TextureInfo text = RM->textures[textureN];

	quadVerts.clear();
	quadUVs.clear();

	quadVerts = {
		coords.Get<0>(), coords.Get<1>(), 0.f,
		coords.Get<0>(), coords.Get<1>() + size.Get<1>(), 0.f,
		coords.Get<0>() + size.Get<0>(), coords.Get<1>() + size.Get<1>(), 0.f,
		coords.Get<0>() + size.Get<0>(), coords.Get<1>(), 0.f
	};

	quadUVs = {
		0.f, 0.f,
		0.f, 1.f,
		1.f, 1.f,
		1.f, 0.f
	};

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, text.bufferID);

	glBindBuffer(GL_ARRAY_BUFFER, fontVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadVerts.size(), &quadVerts[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, fontUV);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadUVs.size(), &quadUVs[0], GL_STATIC_DRAW);

	glDrawArrays(GL_QUADS, 0, quadVerts.size() / 3);
	glUseProgram(0);
}
