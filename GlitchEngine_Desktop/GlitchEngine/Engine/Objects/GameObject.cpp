#include "GameObject.h"
#include <Classes/Renderer.h>

GameObject::GameObject()
{
	tag = "Default";
	transform = new Transform(false, this);
	rigidBody = nullptr;

	uID = Renderer::ObjectUniqueID;
	Renderer::ObjectUniqueID++;
}

GameObject::~GameObject()
{
	vector<GameObject*>::iterator it = find(Renderer::ObjectsWithRigidBody.begin(), Renderer::ObjectsWithRigidBody.end(), this);
	if (it != Renderer::ObjectsWithRigidBody.end())
		Renderer::ObjectsWithRigidBody.erase(it);

	it = find(Renderer::ComponentsUpdate.begin(), Renderer::ComponentsUpdate.end(), this);
	if (it != Renderer::ComponentsUpdate.end())
		Renderer::ComponentsToDeleteAfterUpdate.push_back(this);
	
	isActive = false;
	delete rigidBody;
	delete transform;
}

void GameObject::SendModel()
{
}

void GameObject::SetActive(bool active)
{
	isActive = active;
	if (rigidBody != nullptr)
		rigidBody->SetActive(active);
}

bool GameObject::GetActiveState() const
{
	return isActive;
}

RigidBody* GameObject::AddRigidBody(unsigned int type)
{
	rigidBody = new RigidBody(transform, (b2BodyType)type, this);
	Renderer::ObjectsWithRigidBody.push_back(this);
	return rigidBody;
}

Transform* GameObject::GetTransform()
{
	return transform;
}

RigidBody* GameObject::GetRigidBody()
{
	return rigidBody;
}

int GameObject::GetUID()
{
	return uID;
}

void GameObject::RegisterComponentID(string cID)
{
	componentIDs.push_back(cID);
	componentIDs.erase(unique(componentIDs.begin(), componentIDs.end()), componentIDs.end());

	if (!haveToUpdate)
	{
		vector<GameObject*>::iterator it = find(Renderer::ComponentsToAddAfterUpdate.begin(), Renderer::ComponentsToAddAfterUpdate.end(), this);
		vector<GameObject*>::iterator it2 = find(Renderer::ComponentsUpdate.begin(), Renderer::ComponentsUpdate.end(), this);

		if (it == Renderer::ComponentsToAddAfterUpdate.end() && it2 == Renderer::ComponentsUpdate.end())
		{
			Renderer::ComponentsToAddAfterUpdate.push_back(this);
			haveToUpdate = true;
		}
	}
}

vector<string> GameObject::GetComponentIDs()
{
	return componentIDs;
}

void GameObject::SetTag(string t)
{
	tag = t;
}

string GameObject::GetTag() const
{
	return tag;
}
