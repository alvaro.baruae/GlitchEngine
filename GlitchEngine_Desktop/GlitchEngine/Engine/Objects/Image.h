#pragma once
#include <Objects\Quad.h>

class Image : public Quad
{
public:
	Image();
	Image(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, bool translucent, string image);
	~Image();

	void FlipX(bool flipped);
	void FlipY(bool flipped);
};