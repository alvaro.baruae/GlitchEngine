#include <Objects/Camera.h>
#define PI 3.14159265359

Camera::Camera(Vector<glm::vec2> pos, bool _active = true)
{
	ApplicationData* appData = &ApplicationData::GetInstance();
	cWidth = appData->renderWidth;
	cHeight = appData->renderHeight;
	
	ortographicSize = 1.f;
	projection = glm::ortho(-cWidth / 2.f, cWidth / 2.f, -cHeight / 2.f, cHeight / 2.f, -1.f, 1000.f);
	view = glm::lookAt(glm::vec3(pos.Get<0>(), pos.Get<1>(), 1), glm::vec3(pos.Get<0>(), pos.Get<1>(), 0), glm::vec3(0, 1, 0));
	viewPort = glm::vec4(0, 0, cWidth, cHeight);

	transform = Transform(true, nullptr);
	transform.SetPosition(pos);

	active = _active;
	CamList.push_back(this);
}

void Camera::SetSize(float size)
{
	ortographicSize = size;
	projection = glm::ortho(-cWidth / 2.f / ortographicSize, cWidth / 2.f / ortographicSize, -cHeight / 2.f / ortographicSize, cHeight / 2.f / ortographicSize, -1.f, 1000.f);
	transform.CameraUpdatedSize();
}

float Camera::Size() const
{
	return ortographicSize;
}

glm::mat4 Camera::GetViewMatrix()
{
	return transform.GetMatrix();
}

glm::mat4 Camera::GetProjectionMatrix()
{
	return projection;
}

Transform * Camera::GetTransform()
{
	return &transform;
}

void Camera::ScreenSplit(unsigned short split)
{
	ApplicationData* appData = &ApplicationData::GetInstance();
	Vector<glm::vec4> vp;

	switch (split)
	{
		case TOP:
			vp = Vector<glm::vec4>(0.f, 0.5f, 1.f, 1.f);
			break;
		case BOTTOM:
			vp = Vector<glm::vec4>(0.f, 0.f, 1.f, 0.5f);
			break;
		case LEFT:
			vp = Vector<glm::vec4>(0.f, 0.f, 0.5f, 1.f);
			break;
		case RIGHT:
			vp = Vector<glm::vec4>(0.5f, 0.f, 1.f, 1.f);
			break;
		case TOP_LEFT:
			vp = Vector<glm::vec4>(0.f, 0.5f, 0.5f, 1.f);
			break;
		case TOP_RIGHT:
			vp = Vector<glm::vec4>(0.5f, 0.5f, 1.f, 1.f);
			break;
		case BOTTOM_LEFT:
			vp = Vector<glm::vec4>(0.f, 0.f, 0.5f, 0.5f);
			break;
		case BOTTOM_RIGHT:
			vp = Vector<glm::vec4>(0.5f, 0.f, 1.f, 0.5f);
			break;
	}

	viewPort.x = appData->baseSize.z + vp.Get<0>() * appData->baseSize.x;
	viewPort.z = appData->baseSize.z + vp.Get<2>() * appData->baseSize.x - viewPort.x;

	viewPort.y = appData->baseSize.w + vp.Get<1>() * appData->baseSize.y;
	viewPort.w = appData->baseSize.w + vp.Get<3>() * appData->baseSize.y - viewPort.y;

	float width;
	if (viewPort.z - viewPort.x == 0)
		width = viewPort.z;
	else
		width = (viewPort.z - viewPort.x);

	float height;
	if (viewPort.w - viewPort.y == 0)
		height = viewPort.y;
	else
		height = (viewPort.w - viewPort.y);

	if ((QUARTER_SCREEN & split) ^ split)
	{
		float aspectRatio = width / height;
		if (aspectRatio > 1)
			projection = glm::ortho(-width / 2.f, width / 2.f, -appData->baseSize.y / 2.f / 2.f, appData->baseSize.y / 2.f / 2.f, -1.f, 1000.f);
		else
			projection = glm::ortho(-width / 2.f, width / 2.f, -appData->baseSize.y / 2.f, appData->baseSize.y / 2.f, -1.f, 1000.f);
	}
}

void Camera::SetViewport(Vector<glm::vec4> vp, bool adjustProjection)
{
	ApplicationData* appData = &ApplicationData::GetInstance();

	viewPort.x = vp.Get<0>() * cWidth;
	viewPort.z = vp.Get<2>() * cWidth - viewPort.x;

	viewPort.y = vp.Get<1>() * cHeight;
	viewPort.w = vp.Get<3>() * cHeight - viewPort.y;

	appData->baseSize.x = abs(viewPort.z - viewPort.x);
	appData->baseSize.y = abs(viewPort.w - viewPort.y);
	appData->baseSize.z = viewPort.x;
	appData->baseSize.w = viewPort.y;

	float scaleCorrectionX = viewPort.z / appData->designWidth;
	float scaleCorrectionY = viewPort.w / appData->designHeight;

	appData->aspectScale = Vector<glm::vec2>(scaleCorrectionX, scaleCorrectionY);
	transform.UpdateScaleCorrection();
		
	if (adjustProjection)
		projection = glm::ortho(-viewPort.z / 2.f, viewPort.z / 2.f, -viewPort.w / 2.f, viewPort.w / 2.f, -1.f, 1000.f);
}

glm::vec4 Camera::GetViewport()
{
	return viewPort;
}

bool Camera::IsActive() const
{
	return active;
}

void Camera::SetActive(bool _active)
{
	active = _active;
}

bool Camera::HaveToUpdate()
{
	return transform.UpdateStatus();
}

int Camera::GetWidth() const
{
	return cWidth;
}

int Camera::GetHeight() const
{
	return cHeight;
}