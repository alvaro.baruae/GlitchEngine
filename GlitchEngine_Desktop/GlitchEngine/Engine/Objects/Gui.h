#pragma once
#define TOTALCHARS 96

#include <Classes/Material.h>
#include <Utils/Application.h>
#include <Utils/Input.h>
#include <Utils/LuaEventDispatcher.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <stdio.h>
#include <vector>
#include <map>

typedef pair<string, int> FontName;

struct charInfo
{
	unsigned int advanceX;
	unsigned int advanceY;

	unsigned int bWidth;
	unsigned int bHeight;

	unsigned int bLeft;
	unsigned int bTop;

	float tOffSetX;
	float tOffSetXAdvance;
	float tOffSetYAdvance;
};

struct FontAtlas {

	Material* mat;
	vector<charInfo> chars;
	unsigned textW = 0;
	unsigned textH = 0;

	FontAtlas() {}

	FontAtlas(FT_Face face, int size, string fontName)
	{
		FT_Set_Pixel_Sizes(face, 0, size + 12);
		FT_GlyphSlot g = face->glyph;

		int MAXHEIGHT = INT_MIN;

		for (int i = 32; i < 128; i++)
		{
			FT_Load_Char(face, i, FT_LOAD_RENDER);
			textW += g->bitmap.width;

			if ((int)g->bitmap.rows > MAXHEIGHT)
				MAXHEIGHT = g->bitmap.rows;
		}

		textH = MAXHEIGHT;

		float offSetX = 0;
		for (int i = 32; i < 128; i++)
		{
			FT_Load_Char(face, i, FT_LOAD_RENDER);
			charInfo chari = charInfo();
			chari.advanceX = g->advance.x >> 6;
			chari.advanceY = g->advance.y >> 6;

			chari.bWidth = g->bitmap.width;
			chari.bHeight = g->bitmap.rows;

			chari.bLeft = g->bitmap_left;
			chari.bTop = g->bitmap_top;

			chari.tOffSetX = offSetX / (float)textW;
			chari.tOffSetXAdvance = chari.bWidth / (float)textW;
			chari.tOffSetYAdvance = chari.bHeight / (float)textH;

			chars.push_back(chari);
			offSetX += chari.bWidth;
		}

		mat = new Material("GE_Font", false);
		mat->SetFontTexture(fontName, face, textW, textH);
	}

	~FontAtlas()
	{
		delete mat;
	}
};

class Gui
{
private:
	glm::mat4 guiMat;
	GLuint guiVertexArrayID;
	GLuint fontVBO;
	GLuint fontUV;

	ResourceManager* RM;
	map<FontName, FontAtlas*> fonts;

	glm::vec3 fontColor;
	vector<GLfloat> quadVerts;
	vector<GLfloat> quadUVs;
	Input* input;
	ApplicationData* appData;

	GLuint fontShaderID;
	GLuint buttonShaderID;

	map<GLuint, map<string, GLuint>> uniformLocations;

public:

	static Gui &GetInstance();

	Gui();
	~Gui();
	void SetVertexArray();
	void Destroy();
	void SetFontColor(Vector<glm::vec3> color);
	void Label(const char* text, Vector<glm::vec2> coords, string fontName, int fontSize);
	void Button(Vector<glm::vec2> coords, Vector<glm::vec2> size, string texture);
	void Image(Vector<glm::vec2> coords, Vector<glm::vec2> size, string texture);
};
