#pragma once
#include <map>
#include <vector>
#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Utils/Logger.h>
#include <Classes/RigidBody.h>
#include <Classes/Material.h>

using namespace std;

class Renderer;
class GameObject
{
private:
	string tag;
	vector<string> componentIDs;
	bool isActive = true;
	bool haveToUpdate = false;
	
protected:
	Transform* transform;
	RigidBody* rigidBody;
	int uID = 0;

public:
	GameObject();
	~GameObject();

	virtual void SendModel();
	void SetActive(bool active);
	bool GetActiveState() const;
	Transform* GetTransform();
	RigidBody* GetRigidBody();
	RigidBody* AddRigidBody(unsigned int type);
	int GetUID();
	void RegisterComponentID(string cID);
	vector<string> GetComponentIDs();

	void SetTag(string t);
	string GetTag() const;
};