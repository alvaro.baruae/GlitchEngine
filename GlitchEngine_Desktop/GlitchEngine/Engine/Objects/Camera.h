#pragma once
#define GLM_FORCE_RADIANS

#include <cmath>
#include <string>
#include <vector>
#include <Classes/Transform.h>
#include <Utils/Application.h>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

enum SplitType
{
	TOP = 1 << 0,
	BOTTOM = 1 << 1,
	LEFT = 1 << 2,
	RIGHT = 1 << 3,
	TOP_LEFT = 1 << 4,
	TOP_RIGHT = 1 << 5,
	BOTTOM_LEFT = 1 << 6,
	BOTTOM_RIGHT = 1 << 7,
	QUARTER_SCREEN = TOP_LEFT | TOP_RIGHT | BOTTOM_LEFT | BOTTOM_RIGHT,
};

class Camera
{
    private:
        int cWidth;
        int cHeight;
		float ortographicSize;
		glm::mat4 projection;
		glm::mat4 view;
		Transform transform;
		glm::vec4 viewPort;
		bool active;

    public:
		
		Camera(Vector<glm::vec2> pos, bool active);

		glm::mat4 GetViewMatrix();
		glm::mat4 GetProjectionMatrix();
		Transform *GetTransform();
		void SetViewport(Vector<glm::vec4> vp, bool adjustProjection);
		void ScreenSplit(unsigned short split);
		glm::vec4 GetViewport();
		bool IsActive() const;
		void SetActive(bool _active);
		void SetSize(float size);
		float Size() const;
		bool HaveToUpdate();
		int GetWidth() const;
		int GetHeight() const;

		static std::vector<Camera*> CamList;
		static Camera* GetMainCamera()
		{
			return CamList[0];
		}
};