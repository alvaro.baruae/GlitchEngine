#pragma once
#include <glew.h>
#include <stdio.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <Utils/ShaderLoader.h>
#include <Utils/Application.h>
#include <Utils/ResourceManager.h>

class FrameBuffer
{
private:
	ResourceManager* RM;
	ApplicationData* appData;
	GLuint frameBufferId;
	GLuint textureId;
	GLuint depthStencilId;
	GLuint vertexArrayId;

	GLuint shader;
	bool isEnabled;
	bool includeGUI;

	GLuint fbVBO;
	GLuint fbUV;
	vector<GLfloat> quadVerts;
	vector<GLfloat> quadUVs;
	map<string, GLuint> uniformLocations;

	map<string, int> intMap;
	map<string, float> floatMap;
	map<string, glm::vec2> vec2Map;
	map<string, glm::vec3> vec3Map;
	map<string, glm::vec4> vec4Map;
	map<string, glm::mat4> mat4Map;
	map<string, GLuint> textures;

	void RenderFBO();
	void CheckForUniformLocation(string key);

public:
	static FrameBuffer &GetInstance();
	void Destroy();

	FrameBuffer();
	~FrameBuffer();

	void Activate();
	void Deactivate();
	void SetShader(string s);

	void SetInt(string key, int value);
	void SetFloat(string key, float value);
	void SetVec2(string key, Vector<glm::vec2> value);
	void SetVec3(string key, Vector<glm::vec3> value);
	void SetVec4(string key, Vector<glm::vec4> value);
	void SetTexture(string key, string fileName);

	void SetEnabled(bool state);
	bool IsEnabled() const;

	void SetIncludeGUI(bool state);
	bool IsGuiIncluded() const;
};