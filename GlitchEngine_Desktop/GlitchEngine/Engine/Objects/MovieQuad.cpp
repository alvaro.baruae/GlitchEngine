#include "MovieQuad.h"
#include <Utils/MovieQuadManager.h>

MovieQuad::MovieQuad(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, string fileName, float fps)
{
	this->fps = 1.0 / fps * 1000000000;
	fileLocation = fileName;

	// -- Vorbis Init
	vorbis_info_init(vi);
	vorbis_comment_init(vc);

	// -- Gen Sources
	alGenSources(1, &sourceID);
	alSourcei(sourceID, AL_DISTANCE_MODEL, AL_INVERSE_DISTANCE_CLAMPED);

	glGenTextures(3, texBufferIDs);
	videoThread = thread(&MovieQuad::VideoParse, this);
	audioThread = thread(&MovieQuad::AudioParse, this);
	stop = true;
	hasInit = false;
	atomic_store(&exitThread, false);
	atomic_store(&videoParseFinished, false);
	atomic_store(&audioParseFinished, false);
	exitThread = false;

	material = new Material("GE_Movie", false);
	while (videoDataQueue.size() == 0) {}

	GenerateTextureBuffers();
	transform->Translate(position, Space::WorldSpace);
	transform->Rotate(rotation);
	transform->ScaleObject(size);
	transform->SetLayer(sLayer);

	if (material != nullptr)
	{
		int arrayPos = material->GetArrayPosition();
		if (arrayPos != -1)
		{
			AddToRendererList(arrayPos);
			material->AddParentPosition(positionInGroup);
		}
		else
		{
			Logger::Log("MovieQuad Error!: Make sure to set Material textures before initializing.");
			delete this;
		}
	}

	MovieQuadManager::AddToList(this);
}

MovieQuad::~MovieQuad()
{
	stop = true;
	atomic_store(&exitThread, true);

	while (!atomic_load(&videoParseFinished)) {}
	if(videoThread.joinable())
		videoThread.join();

	while (!atomic_load(&audioParseFinished)) {}
	if (audioThread.joinable())
		audioThread.join();

	delete vi;
	delete vc;
	delete vd;
	delete vb;

	glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[material->GetArrayPosition()]["Model"]);
	glBufferSubData(GL_ARRAY_BUFFER, positionInGroup * sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(glm::mat4(0.f)));
	delete material;

	while (videoDataQueue.size() != 0)
	{
		VideoData* curretFrame = videoDataQueue.front();
		for (int i = 0; i < 3; i++)
			free((*curretFrame)[i]);
		videoDataQueue.pop();
		delete curretFrame;
	}

	glDeleteTextures(3, bufferIDs);
}

void MovieQuad::VideoParse()
{
	// -- MKV Parser
	long long pos = 0;
	int audioTrackNum = -1;
	int videoTrackNum = -1;
	MkvReader reader;
	EBMLHeader ebmlHeader;
	Segment* segment;

	string fullLocation = "Resources//" + fileLocation;
	if (reader.Open(fullLocation.c_str()))
	{
		Logger::Log("MovieQuad Video Error: Filename is invalid or error while opening.");
		return;
	}

	long long ret = ebmlHeader.Parse(&reader, pos);
	if (ret < 0)
	{
		Logger::Log("MovieQuad Video Error: EBMLHeader::Parse() failed.");
		return;
	}

	ret = Segment::CreateInstance(&reader, pos, segment);
	if (ret)
	{
		Logger::Log("MovieQuad Video Error: Segment::CreateInstance() failed.");
		return;
	}

	ret = segment->Load();
	if (ret < 0)
	{
		Logger::Log("MovieQuad Video Error: Segment::Load() failed.");
		return;
	}

	const Tracks* tracks = segment->GetTracks();

	unsigned long track_num = 0;
	const unsigned long num_tracks = tracks->GetTracksCount();

	while (track_num != num_tracks)
	{
		const Track* const track = tracks->GetTrackByIndex(track_num++);

		if (track == NULL)
			continue;

		const long trackType = track->GetType();
		const long trackNumber = track->GetNumber();

		const char* const codecID = track->GetCodecId();

		if (trackType == Track::kVideo)
		{
			const VideoTrack* const videoTrack = static_cast<const VideoTrack*>(track);
			if (videoTrack && !strcmp(codecID, "V_VP8"))
				videoTrackNum = trackNumber;
		}
	}

	const unsigned long clusterCount = segment->GetCount();
	const Cluster* cluster = segment->GetFirst();

	if (videoTrackNum >= 0)
	{
		if (!vpxSetup)
		{
			const vpx_codec_iface_t *iface = vpx_codec_vp8_dx();
			vpx_codec_ctx_t &decoder = vpxDecoder;

			if (iface != NULL)
			{
				const VideoTrack* videoTrack = static_cast<const VideoTrack*>(tracks->GetTrackByNumber(videoTrackNum));
				vpx_codec_dec_cfg_t config;
				config.threads = 1;
				config.w = w = videoTrack->GetWidth();
				config.h = h = videoTrack->GetHeight();
				hW = w / 2;
				hH = h / 2;
				const vpx_codec_flags_t flags = VPX_CODEC_USE_FRAME_THREADING;
				vpx_codec_err_t codecError = vpx_codec_dec_init(&decoder, iface, &config, flags);

				if (codecError == VPX_CODEC_OK)
					vpxSetup = true;
			}
		}

		while ((cluster != NULL) && !cluster->EOS())
		{
			const BlockEntry* blockEntry;
			long status = cluster->GetFirst(blockEntry);

			if (status < 0)
			{
				Logger::Log("MovieQuad Video Error: Error parsing first block of cluster");
				return;
			}

			while ((blockEntry != NULL) && !blockEntry->EOS())
			{
				const Block* const block = blockEntry->GetBlock();
				long long trackNumber = block->GetTrackNumber();

				if (trackNumber == videoTrackNum)
				{
					if (!vpxSetup)
					{
						Logger::Log("MovieQuad Video Error: VPX is not setup");
						return;
					}

					int frameCount = block->GetFrameCount();
					for (int i = 0; i < frameCount; i++)
					{
						const Block::Frame& frame = block->GetFrame(i);
						unsigned char* frameData = (unsigned char*)malloc(frame.len);
						frame.Read(&reader, frameData);

						const vpx_codec_err_t decodeError = vpx_codec_decode(&vpxDecoder, frameData, frame.len, NULL, 0);

						if (decodeError == VPX_CODEC_OK)
						{
							vpx_codec_iter_t iter = NULL;
							vpx_image_t *img = vpx_codec_get_frame(&vpxDecoder, &iter);

							if (img)
							{
								VideoData* data = new VideoData();

								data->planeY = (unsigned char*)malloc(img->d_h * img->d_w);
								data->planeU = (unsigned char*)malloc(img->d_h * img->d_w / 2);
								data->planeV = (unsigned char*)malloc(img->d_h * img->d_w / 2);

								unsigned char* planeY = img->planes[VPX_PLANE_Y];
								unsigned char* planeU = img->planes[VPX_PLANE_U];
								unsigned char* planeV = img->planes[VPX_PLANE_V];

								unsigned char* dstY = data->planeY;
								unsigned char* dstU = data->planeU;
								unsigned char* dstV = data->planeV;

								for (unsigned int imgY = 0; imgY < img->d_h; imgY++)
								{
									memcpy(dstY, planeY + imgY * img->stride[VPX_PLANE_Y], img->d_w);
									dstY += img->d_w;
								}

								int hW = img->d_w / 2;
								int hH = img->d_h / 2;

								for (unsigned int imgY = 0; imgY < hH; imgY++)
								{
									memcpy(dstU, planeU + imgY * img->stride[VPX_PLANE_U], hW);
									dstU += hW;

									memcpy(dstV, planeV + imgY * img->stride[VPX_PLANE_V], hW);
									dstV += hW;
								}

								while (videoDataQueue.size() >= MAX_TEXTURE_UNITS) {
									if (atomic_load(&exitThread))
										break;
								}

								if (atomic_load(&exitThread))
								{
									delete data;
									vpx_img_free(img);
									break;
								}	

								videoDataQueue.push(data);
								vpx_img_free(img);
							}
							else
								assert(false);
						}
						free(frameData);
					}

					if (atomic_load(&exitThread))
						break;
				}

				if (atomic_load(&exitThread))
					break;

				status = cluster->GetNext(blockEntry, blockEntry);

				if (status < 0)
				{
					Logger::Log("MovieQuad Video Error: Error parsing next block of cluster");
					return;
				}
			}

			cluster = segment->GetNext(cluster);
		}
	}

	delete segment;
	vpx_codec_destroy(&vpxDecoder);
	reader.Close();
	atomic_store(&videoParseFinished, true);
}

void MovieQuad::AudioParse()
{
	// -- MKV Parser
	long long pos = 0;
	int audioTrackNum = -1;
	int videoTrackNum = -1;
	MkvReader reader;
	EBMLHeader ebmlHeader;
	Segment* segment;

	blockBuffer.reserve(blockBufferSize);
	string fullLocation = "Resources//" + fileLocation;
	if (reader.Open(fullLocation.c_str()))
	{
		Logger::Log("MovieQuad Audio Error: Filename is invalid or error while opening.");
		return;
	}

	long long ret = ebmlHeader.Parse(&reader, pos);
	if (ret < 0)
	{
		Logger::Log("MovieQuad Audio Error: EBMLHeader::Parse() failed.");
		return;
	}

	ret = Segment::CreateInstance(&reader, pos, segment);
	if (ret)
	{
		Logger::Log("MovieQuad Audio Error: Segment::CreateInstance() failed.");
		return;
	}

	ret = segment->Load();
	if (ret < 0)
	{
		Logger::Log("MovieQuad Audio Error: Segment::Load() failed.");
		return;
	}

	const Tracks* tracks = segment->GetTracks();

	unsigned long track_num = 0;
	const unsigned long num_tracks = tracks->GetTracksCount();

	while (track_num != num_tracks)
	{
		const Track* const track = tracks->GetTrackByIndex(track_num++);

		if (track == NULL)
			continue;

		const long trackType = track->GetType();
		const long trackNumber = track->GetNumber();

		const char* const codecID = track->GetCodecId();

		if (trackType == Track::kAudio)
		{
			const AudioTrack* const audioTrack = static_cast<const AudioTrack*>(track);
			if (audioTrack && !strcmp(codecID, "A_VORBIS"))
				audioTrackNum = trackNumber;
		}
	}

	const unsigned long clusterCount = segment->GetCount();
	const Cluster* cluster = segment->GetFirst();

	if (audioTrackNum >= 0)
	{
		if (!vorbisSetup)
		{
			size_t size;
			unsigned char* vorbisHeaderData = (unsigned char*)tracks->GetTrackByNumber(audioTrackNum)->GetCodecPrivate(size);

			ogg_packet* op = new ogg_packet();

			for (int i = 0; i < 3; i++)
			{
				while (memcmp(vorbisHeaderData, "vorbis", 6) != 0)
				{
					vorbisHeaderData++;
					size--;
				}

				op->packet = vorbisHeaderData;
				op->packet--;
				op->bytes = size;
				op->b_o_s = op->packetno == 0;

				int ret = vorbis_synthesis_headerin(vi, vc, op);
				if (ret < 0)
				{
					Logger::Log("MovieQuad Audio Error: vorbis_synthesis_headerin failed, error: %d", ret);
					return;
				}

				op->packetno++;
				vorbisHeaderData++;
			}

			int ret = vorbis_synthesis_init(vd, vi);
			if (ret < 0)
			{
				Logger::Log("MovieQuad Audio Error: vorbis_synthesis_init failed, error: %d", ret);
				return;
			}

			ret = vorbis_block_init(vd, vb);
			if (ret < 0)
			{
				Logger::Log("MovieQuad Audio Error: vorbis_block_init failed, error: %d", ret);
				return;
			}

			vorbisSetup = true;
		}

		while ((cluster != NULL) && !cluster->EOS())
		{
			const BlockEntry* blockEntry;
			long status = cluster->GetFirst(blockEntry);

			if (status < 0)
			{
				Logger::Log("MovieQuad Audio Error: Error parsing first block of cluster");
				return;
			}

			while ((blockEntry != NULL) && !blockEntry->EOS())
			{
				const Block* const block = blockEntry->GetBlock();
				long long trackNumber = block->GetTrackNumber();

				if (trackNumber == audioTrackNum)
				{
					if (!vorbisSetup)
					{
						Logger::Log("MovieQuad Audio Error: Vorbis is not setup");
						return;
					}

					ogg_packet packet;
					packet.packet = NULL;
					packet.bytes = 0;
					packet.b_o_s = false;
					packet.e_o_s = false;
					packet.granulepos = -1;
					packet.packetno = 0;

					int frameCount = block->GetFrameCount();
					for (int i = 0; i < frameCount; i++)
					{
						const Block::Frame& frame = block->GetFrame(i);
						unsigned char* frameData = (unsigned char*)malloc(frame.len);
						frame.Read(&reader, frameData);

						packet.packet = frameData;
						packet.bytes = frame.len;
						packet.packetno++;

						int ret = vorbis_synthesis(vb, &packet);
						if (ret < 0)
						{
							Logger::Log("MovieQuad Audio Error: vorbis_synthesis failed, error: %d", ret);
							return;
						}

						ret = vorbis_synthesis_blockin(vd, vb);
						if (ret < 0)
						{
							Logger::Log("MovieQuad Audio Error: vorbis_synthesis_blockin failed, error: %d", ret);
							return;
						}

						float **pcm = NULL;
						int samples = 0;
						while ((samples = vorbis_synthesis_pcmout(vd, &pcm)) > 0)
						{
							vector<short> tempBuffer;
							tempBuffer.resize(samples * vi->channels);

							for (int ch = 0; ch < vi->channels; ch++)
							{
								int j = 0;
								const float* const one_channel = pcm[ch];
								for (int i = ch; i < samples * vi->channels; i += vi->channels)
									tempBuffer[i] = static_cast<short>(one_channel[j++] * 32767.0f);
							}

							if (blockBuffer.size() + tempBuffer.size() <= blockBufferSize)
								blockBuffer.insert(blockBuffer.end(), tempBuffer.begin(), tempBuffer.end());
							else
							{
								audioDataQueue.push(blockBuffer);
								blockBuffer.clear();
								blockBuffer.insert(blockBuffer.end(), tempBuffer.begin(), tempBuffer.end());
							}

							if (atomic_load(&exitThread))
								break;

							vorbis_synthesis_read(vd, samples);
						}

						free(frameData);
					}

					if (atomic_load(&exitThread))
						break;
				}

				if (atomic_load(&exitThread))
					break;

				status = cluster->GetNext(blockEntry, blockEntry);

				if (status < 0)
				{
					Logger::Log("MovieQuad Audio Error: Error parsing next block of cluster");
					fflush(stdout);
					return;
				}
			}

			cluster = segment->GetNext(cluster);
		}
	}

	delete segment;
	reader.Close();
	atomic_store(&audioParseFinished, true);
}

void MovieQuad::GnerateTextures()
{
	VideoData* curretFrame = videoDataQueue.front();
	for (int i = 0; i < 3; i++)
	{
		glBindTexture(GL_TEXTURE_2D, texBufferIDs[i]);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, i == 0 ? w : hW, i == 0 ? h : hH, GL_RED, GL_UNSIGNED_BYTE, (*curretFrame)[i]);
		free((*curretFrame)[i]);
	}

	videoDataQueue.pop();
	delete curretFrame;
}

void MovieQuad::GenerateTextureBuffers()
{
	VideoData* curretFrame = videoDataQueue.front();
	for (int i = 0; i < 3; i++)
	{
		glBindTexture(GL_TEXTURE_2D, texBufferIDs[i]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, i == 0 ? w : hW, i == 0 ? h : hH, 0, GL_RED, GL_UNSIGNED_BYTE, (*curretFrame)[i]);
		free((*curretFrame)[i]);
	}

	material->SetMovieTextures(texBufferIDs[0], texBufferIDs[1], texBufferIDs[2]);
	videoDataQueue.pop();
	delete curretFrame;
}

void MovieQuad::MovieUpdateLoop(double deltaTime)
{
	if (!stop)
	{
		counter += deltaTime;
		if (!bufferGenerated && vorbisSetup && audioDataQueue.size() >= MAX_QUEUE_BUFFERS && videoDataQueue.size() >= MAX_TEXTURE_UNITS)
		{
			switch (vi->channels)
			{
			case 1:
				format = AL_FORMAT_MONO16;
				break;
			case 2:
				format = AL_FORMAT_STEREO16;
				break;
			}

			alGenBuffers(MAX_QUEUE_BUFFERS, bufferIDs);
			for (int i = 0; i < MAX_QUEUE_BUFFERS; i++)
			{
				alBufferData(bufferIDs[i], format, &audioDataQueue.front()[0], audioDataQueue.front().size() * sizeof(short), vi->rate);
				alSourceQueueBuffers(sourceID, 1, &bufferIDs[i]);
				audioDataQueue.pop();
			}

			bufferGenerated = true;
			counter = 0;
		}

		if (bufferGenerated)
		{
			if (audioStarted && counter >= fps && videoDataQueue.size() > 0)
			{
				genNextFrame = true;
				counter -= fps;
			}
			else if (genNextFrame && counter < fps)
			{
				GnerateTextures();
				genNextFrame = false;
			}

			if (audioDataQueue.size() > 0)
			{
				ALint processedBuffers = 0;
				alGetSourcei(sourceID, AL_BUFFERS_PROCESSED, &processedBuffers);
				while (processedBuffers && audioDataQueue.size() > 0)
				{
					unsigned int dequeuedBufferID = 0;
					alSourceUnqueueBuffers(sourceID, 1, &dequeuedBufferID);
					alBufferData(dequeuedBufferID, format, &audioDataQueue.front()[0], audioDataQueue.front().size() * sizeof(short), vi->rate);
					alSourceQueueBuffers(sourceID, 1, &dequeuedBufferID);
					processedBuffers--;
					audioDataQueue.pop();
				}
			}

			ALenum state;
			alGetSourcei(sourceID, AL_SOURCE_STATE, &state);
			if (state != AL_PLAYING)
			{
				ALint queuedBuffers;
				alGetSourcei(sourceID, AL_BUFFERS_QUEUED, &queuedBuffers);
				if (queuedBuffers)
				{
					alSourcePlay(sourceID);
					audioStarted = true;
				}
			}

			if (atomic_load(&audioParseFinished) && atomic_load(&videoParseFinished) && audioDataQueue.size() == 0)
			{
				alSourceStop(sourceID);
				alDeleteBuffers(MAX_QUEUE_BUFFERS, bufferIDs);
				stop = true;
			}
		}

		if (atomic_load(&audioParseFinished) && audioThread.joinable())
			audioThread.join();

		if (atomic_load(&videoParseFinished) && videoThread.joinable())
			videoThread.join();
	}
}

void MovieQuad::Play()
{
	stop = false;
	hasInit = true;
}

void MovieQuad::Stop()
{
	stop = true;
	atomic_store(&exitThread, true);
}

bool MovieQuad::GetStatus()
{
	return stop;
}

bool MovieQuad::HasStartedPlaying()
{
	return hasInit;
}