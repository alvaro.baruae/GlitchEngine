#pragma once
#define QUEUEBUFFERS 4

#include <vector>
#include <Utils/ResourceManager.h>
#include <Utils/Vector.h>
#include <Objects/SoundManager.h>
#include <glm/glm.hpp>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>
#include <AL/al.h>

using namespace std;

class SoundManager
{
public:
	class SoundSource
	{
	private:
		SoundInfo currentSound;
		string soundName;
		unsigned int sourceID;
		ResourceManager *RM;

		FILE *p;
		vorbis_info *vi;
		OggVorbis_File vf;
		ALenum format;
		unsigned long buffSize;
		char* decodedBuffer;
		bool loop;

		unsigned long DecodeOGG();
		void PlayVorbisLoop();


	public:
		SoundSource(string name);
		~SoundSource();

		bool process;
		void Play(bool loop);
		void Stop();
		void Pause();
		void ReSet(bool loop);
		void SetPosition(Vector<glm::vec2> pos);
		void SetVelocity(Vector<glm::vec2> vel);	//Does not affect position (Doppler effect calcuation only)
		void SetVolume(float gain);					//Defaults to 1 if not Set
		void SetRollOffFactor(float factor);		//if 0 no attenuation is applied, Defaults to 1
		void SetPitch(float pitch);
		void SetReferenceDistance(float dist);		// 0 - Ref Distance (Gain Clamped)
		void SetMaxDistance(float dist);			//Max Attenuation Distance
		void ProcessStream();
	};

private:
	static vector<SoundSource*> ProcessingList;

public:
	SoundManager();
	~SoundManager();

	static void AddToSourceProcessingList(SoundSource* source)
	{
		vector<SoundSource*>::iterator it = find(ProcessingList.begin(), ProcessingList.end(), source);
		if (it != ProcessingList.end())
			source->process = true;
		else
		{
			source->process = true;
			ProcessingList.push_back(source);
		}
	}

	static void RemoveFromSourceProcessingList(SoundSource* source)
	{
		vector<SoundSource*>::iterator it = find(ProcessingList.begin(), ProcessingList.end(), source);
		
		if (it != ProcessingList.end())
			source->process = false;
	}

	static void ProcessSourcesList()
	{
		for (auto const& it : ProcessingList)
		{
			if (it->process)
				it->ProcessStream();
		}
	}
};

