#pragma once
#include <Objects/GameObject.h>

class Quad : public GameObject
{
private:
	bool isVisible = true;

protected:
	Material* material;
	int positionInGroup;
	virtual void SendModel();
	void AddToRendererList(GLuint shader);
	void SetMaterial(Material* mat);

public:
	Quad();
	Quad(Vector<glm::vec2> position, float rotation, Vector<glm::vec2> size, short sLayer, Material* mat);
	~Quad();

	void SetVisibility(bool visible);
	bool GetVisibility() const;
	Material* GetMaterial() const;
	short GetSortingLayer() const;
	void SetSortingLayer(short layer);
};