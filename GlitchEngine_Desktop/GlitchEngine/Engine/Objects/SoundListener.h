#pragma once

#include <AL/al.h>
#include <Utils/Vector.h>

class SoundListener
{
public:

	SoundListener();
	~SoundListener();
	
	static void SetPosition(Vector<glm::vec2> pos)
	{
		alListener3f(AL_POSITION, pos.Get<0>(), pos.Get<1>(), 0.f);
	}

	static void SetOrientation()
	{
		alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);
		float orientation[] = {
			0.f, 0.f, -1.f,
			0.f, 1.f,  0.f
		};
		alListenerfv(AL_POSITION, orientation);
	}

	static void SetVelocity(Vector<glm::vec2> vel)
	{
		alListener3f(AL_VELOCITY, vel.Get<0>(), vel.Get<1>(), 0.f);
	}

	static void SetGain(float gain)
	{
		alListenerf(AL_GAIN, gain);
	}

};

