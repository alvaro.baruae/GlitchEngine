#include <Classes/LightScreen.h>


LightScreen::LightScreen()
{
	appData = &ApplicationData::GetInstance();
	transform = Transform(false, nullptr);

	quadVerts = {
		-0.5f, 0.5f, 0.f,
		-0.5f, -0.5f, 0.f,
		0.5f, -0.5f, 0.f,
		0.5f, 0.5f, 0.f
	};

	glGenVertexArrays(1, &vertexArray);
	glBindVertexArray(vertexArray);

	glGenBuffers(1, &vertexBuffer);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * quadVerts.size(), &quadVerts[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

	shaderID = ShaderLoader::GetProgramIDAt("GE_LightScreen");
	SetAmbientColor(appData->ambientColor);
	transform.SetScale(Vector<glm::vec2>(appData->renderWidth, appData->renderHeight));
	transform.SetLayer(1);

	uniformLocations["ViewProj"] = glGetUniformLocation(shaderID, "ViewProj");
	uniformLocations["Model"] = glGetUniformLocation(shaderID, "Model");
	uniformLocations["numberOfLights"] = glGetUniformLocation(shaderID, "numberOfLights");

	for (unsigned int i = 0; i < 20; i++)
	{
		string uniformName = "sources[" + std::to_string(i) + "].lightPos";
		uniformLocations[uniformName] = glGetUniformLocation(shaderID, uniformName.c_str());

		uniformName = "sources[" + std::to_string(i) + "].radious";
		uniformLocations[uniformName] = glGetUniformLocation(shaderID, uniformName.c_str());

		uniformName = "sources[" + std::to_string(i) + "].color";
		uniformLocations[uniformName] = glGetUniformLocation(shaderID, uniformName.c_str());
		
		uniformName = "sources[" + std::to_string(i) + "].brightness";
		uniformLocations[uniformName] = glGetUniformLocation(shaderID, uniformName.c_str());
	}

	glBindVertexArray(0);
}


LightScreen::~LightScreen()
{
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteVertexArrays(1, &vertexArray);
}

void LightScreen::AddToLightSources(LightSource* source)
{
	lightSources.push_back(source);
}

void LightScreen::DrawScreen(Camera* cam)
{
	vector<LightSource*> onScreenSources;
	glBindVertexArray(vertexArray);
	glUseProgram(shaderID);

	if (cam->HaveToUpdate())
	{
		glUniformMatrix4fv(uniformLocations["ViewProj"], 1, GL_FALSE, glm::value_ptr(cam->GetProjectionMatrix() * cam->GetViewMatrix()));
		cam->GetTransform()->ResetUpdateStatus();

		Vector<glm::vec2> camPos = cam->GetTransform()->WorldPosition();
		transform.SetPosition(camPos);
		transform.SetScale(Vector<glm::vec2>(appData->renderWidth / cam->Size(), appData->renderHeight / cam->Size()));
		Vector<glm::vec2> scaleFactor = (&ApplicationData::GetInstance())->aspectScale;

		glUniformMatrix4fv(uniformLocations["Model"], 1, GL_FALSE, glm::value_ptr(glm::scale(transform.GetMatrix(), glm::vec3(scaleFactor.Get<0>(), scaleFactor.Get<1>(), 1.f))));
	}

	for (unsigned int i = 0; i < lightSources.size(); i++)
	{
		if (lightSources[i]->active)
		{
			Vector<glm::vec2> topLeft = lightSources[i]->position + Vector<glm::vec2>(-lightSources[i]->radious, lightSources[i]->radious);
			Vector<glm::vec2> topRight = lightSources[i]->position + Vector<glm::vec2>(lightSources[i]->radious, lightSources[i]->radious);
			Vector<glm::vec2> botLeft = lightSources[i]->position + Vector<glm::vec2>(-lightSources[i]->radious, -lightSources[i]->radious);
			Vector<glm::vec2> botRight = lightSources[i]->position + Vector<glm::vec2>(lightSources[i]->radious, -lightSources[i]->radious);

			if (PointInsideScreen(topLeft, transform.LocalPosition()) || PointInsideScreen(topRight, transform.LocalPosition()) 
				|| PointInsideScreen(botLeft, transform.LocalPosition()) || PointInsideScreen(botRight, transform.LocalPosition()))
				if (onScreenSources.size() < appData->maxLightsOnScreen)
					onScreenSources.push_back(lightSources[i]);
		}
	}

	if (lastNumberOfLights != onScreenSources.size())
	{
		glUniform1i(uniformLocations["numberOfLights"], onScreenSources.size());
		lastNumberOfLights = onScreenSources.size();
	}

	for (unsigned int i = 0; i < onScreenSources.size(); i++)
	{
		if (onScreenSources[i]->positionNeedsUpdate)
			glUniform2f(uniformLocations["sources[" + std::to_string(i) + "].lightPos"], onScreenSources[i]->position.Get<0>(), onScreenSources[i]->position.Get<1>());
		
		if (onScreenSources[i]->radiousNeedsUpdate)
			glUniform1f(uniformLocations["sources[" + std::to_string(i) + "].radious"], onScreenSources[i]->radious);
		
		if (onScreenSources[i]->colorNeedsUpdate)
			glUniform3f(uniformLocations["sources[" + std::to_string(i) + "].color"], onScreenSources[i]->color.Get<0>(), onScreenSources[i]->color.Get<1>(), onScreenSources[i]->color.Get<2>());
		
		if (onScreenSources[i]->brightnessNeedsUpdate)
			glUniform1f(uniformLocations["sources[" + std::to_string(i) + "].brightness"], onScreenSources[i]->brightness);

		onScreenSources[i]->ResetUpdateStatus();
	}

	glDrawArrays(GL_QUADS, 0, 4);
	glBindVertexArray(0);
	glUseProgram(0);
}

LightScreen &LightScreen::GetInstance()
{
	static LightScreen* ins;
	if (ins == NULL)
		ins = new LightScreen();
	return *ins;
}

void LightScreen::Destroy()
{
	delete &GetInstance();
}

bool LightScreen::PointInsideScreen(Vector<glm::vec2> point, Vector<glm::vec2> camPos)
{
	if (point.Get<0>() >= camPos.Get<0>() - appData->renderWidth / 2 && point.Get<0>() <= camPos.Get<0>() + appData->renderWidth / 2 
		&& point.Get<1>() >= camPos.Get<1>() - appData->renderHeight / 2 && point.Get<1>() <= camPos.Get<1>() + appData->renderHeight / 2)
		return true;
	return false;
}

void LightScreen::SetAmbientColor(Vector<glm::vec3> color)
{
	glUseProgram(shaderID);
	glUniform3f(glGetUniformLocation(shaderID, "ambientColor"), color.Get<0>(), color.Get<1>(), color.Get<2>());
	glUseProgram(0);
}