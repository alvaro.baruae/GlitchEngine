#include <Classes/Transform.h>
#include <Classes/Renderer.h>

Transform::Transform(bool isCam, GameObject* p)
{
	scaleCorrection = (&ApplicationData::GetInstance())->aspectScale;
	cameraFix = isCam ? -1 : 1;
	localScale = Vector<glm::vec2>(1.f, 1.f);
	localRotation = 0.f;

	translationMatrix = glm::mat4(1.f);
	rotationMatrix = glm::mat4(1.f);
	scaleMatrix = glm::mat4(1.f);
	gameObject = p;
	parentTransform = nullptr;
	haveToUpdate = false;

	CheckForUpdate();
}

void Transform::ResetCameraScale()
{
	if (cameraFix == -1)
	{
		Vector<glm::vec2> scaleVector(1.f, 1.f);
		scaleMatrix = glm::scale(glm::mat4(1.f), glm::vec3(scaleVector.Get<0>(), scaleVector.Get<1>(), 1.f));
		localScale = scaleVector;
		CheckForUpdate();
	}
}

Transform::Transform()
{
	cameraFix = 1;
	scaleCorrection = (&ApplicationData::GetInstance())->aspectScale;
	localScale = Vector<glm::vec2>(1.f, 1.f);
	localRotation = 0.f;

	translationMatrix = glm::mat4(1.f);
	rotationMatrix = glm::mat4(1.f);
	scaleMatrix = glm::mat4(1.f);
	gameObject = nullptr;
	parentTransform = nullptr;
	haveToUpdate = false;

	CheckForUpdate();
}

Transform::~Transform()
{
	if (parentTransform != nullptr)
		parentTransform->RemoveChild(this);
}

float Transform::LocalRotation() const
{
	return localRotation;
}

float Transform::WorldRotation() const
{
	if(parentTransform != nullptr)
		return worldRotation;
	return localRotation;
}

Vector<glm::vec2> Transform::LocalScale() const
{
	return localScale;
}

Vector<glm::vec2> Transform::WorldScale() const
{
	return worldScale;
}

void Transform::Rotate(float angle)
{
	rotationMatrix = glm::rotate(rotationMatrix, glm::radians(angle), glm::vec3(0, 0, -1));
	localRotation += angle;
	CheckForUpdate();
}

Vector<glm::vec2> Transform::LocalPosition() const
{
	return Vector<glm::vec2>(translationMatrix[3][0] * cameraFix, translationMatrix[3][1] * cameraFix) / scaleCorrection;
}

Vector<glm::vec2> Transform::WorldPosition() const
{
	if (parentTransform != nullptr)
		return worldPosition;
	else
		return LocalPosition();
}

void Transform::SetPosition(Vector<glm::vec2> newPos)
{
	newPos *= scaleCorrection;
	translationMatrix[3][0] = cameraFix * newPos.Get<0>() / (parentTransform != nullptr ? parentTransform->worldScale.Get<0>() : 1.f);
	translationMatrix[3][1] = cameraFix * newPos.Get<1>() / (parentTransform != nullptr ? parentTransform->worldScale.Get<1>() : 1.f);
	CheckForUpdate();

	if (gameObject != nullptr)
	{
		RigidBody* rb = gameObject->GetRigidBody();
		if (rb != nullptr)
			rb->SetTransform(newPos, worldRotation);
	}
}

void Transform::SetPositionFromRigidBody(Vector<glm::vec2> newPos)
{
	newPos *= scaleCorrection;
	translationMatrix[3][0] = cameraFix * newPos.Get<0>() / (parentTransform != nullptr ? parentTransform->worldScale.Get<0>() : 1.f);
	translationMatrix[3][1] = cameraFix * newPos.Get<1>() / (parentTransform != nullptr ? parentTransform->worldScale.Get<1>() : 1.f);
	CheckForUpdate();
}

void Transform::SetRotation(float rot)
{
	rotationMatrix = glm::rotate(glm::mat4(1.f), glm::radians(rot), glm::vec3(0, 0, -1));
	localRotation = rot;
	CheckForUpdate();

	RigidBody* rb = gameObject->GetRigidBody();
	if (rb != nullptr)
		rb->SetTransform(worldPosition, worldRotation);
}

void Transform::SetRotationFromRigidBody(float rot)
{
	rotationMatrix = glm::rotate(glm::mat4(1.f), glm::radians(rot), glm::vec3(0, 0, -1));
	localRotation = rot;
	CheckForUpdate();
}

void Transform::SetScale(Vector<glm::vec2> scaleVector)
{
	if (cameraFix != -1 || (cameraFix == -1 && parentTransform))
	{
		scaleMatrix = glm::scale(glm::mat4(1.f), glm::vec3(scaleVector.Get<0>(), scaleVector.Get<1>(), 1.f));
		localScale = scaleVector;
		CheckForUpdate();
	}
}

void Transform::Translate(Vector<glm::vec2> newPos, unsigned short space = 0)
{
	newPos *= scaleCorrection;
	if (newPos != Vector<glm::vec2>(0, 0))
	{
		if (space == WorldSpace)
			translationMatrix = glm::translate(translationMatrix, glm::vec3(newPos.Get<0>(), newPos.Get<1>(), 0.f));
		else
		{
			glm::vec3 direction = glm::rotate(glm::vec3(newPos.Get<0>(), newPos.Get<1>(), 0.f), glm::radians(localRotation), glm::vec3(0, 0, -1));
			translationMatrix = glm::translate(translationMatrix, direction);
		}

		CheckForUpdate();
	}
}

void Transform::ScaleObject(Vector<glm::vec2> scaleVector)
{
	if (scaleVector != Vector<glm::vec2>(1, 1))
	{
		if (cameraFix != -1)
		{
			scaleMatrix = glm::scale(scaleMatrix, glm::vec3(scaleVector.Get<0>(), scaleVector.Get<1>(), 1.f));
			localScale = Vector<glm::vec2>(localScale.Get<0>() * scaleVector.Get<0>(), localScale.Get<1>() * scaleVector.Get<1>());
			CheckForUpdate();
		}
	}
}

glm::mat4 Transform::GetMatrix()
{
	if (parentTransform)
	{
		glm::mat4 localMatrix = (translationMatrix * rotationMatrix * scaleMatrix);
		localMatrix[3][2] = 0;

		glm::mat4 parentMatrix = parentTransform->GetMatrix();
		parentMatrix[3][2] = 0;
		worldMatrix = parentMatrix * localMatrix;
		worldMatrix[3][2] = translationMatrix[3][2];
		DecomposeWorldMAtrix();

		if (cameraFix == -1)
		{
			worldMatrix[3][0] = -worldMatrix[3][0];
			worldMatrix[3][1] = -worldMatrix[3][1];
		}

		return worldMatrix;
	}
	else
	{
		DecomposeWorldMAtrix();
		return translationMatrix * rotationMatrix * scaleMatrix;
	}
}

void Transform::SetParent(Transform * p)
{
	if (p == parentTransform) return;
	if (parentTransform != nullptr || p == nullptr)
	{
		vector<Transform*>::iterator it = find(parentTransform->children.begin(), parentTransform->children.end(), this);
		parentTransform->children.erase(it);
		parentTransform = nullptr;
	}

	if (p)
	{
		worldRotation = LocalRotation();
		worldScale = LocalScale();
		worldPosition = LocalPosition();

		parentTransform = p;
		p->children.push_back(this);

		parentTransform->DecomposeWorldMAtrix();
		Vector<glm::vec2> newScale = worldScale / parentTransform->worldScale;

		Vector<glm::vec2> direction;
		if (cameraFix == -1)
			direction = (parentTransform->worldPosition - worldPosition);
		else
			direction = (worldPosition - parentTransform->worldPosition);

		glm::vec3 newPos = glm::rotate(glm::vec3(direction.Get<0>(), direction.Get<1>(), 0.f), glm::radians(-parentTransform->worldRotation), glm::vec3(0, 0, -1));

		SetRotation(worldRotation - parentTransform->worldRotation);
		SetScale(newScale);
		SetPosition(Vector<glm::vec2>(newPos.x, newPos.y));
		GetMatrix();
	}
	else
		RevertLocalMatrix();
}

int Transform::ChildCount()
{
	return children.size();
}

Transform* Transform::GetChildAt(int index)
{
	if (children.size() >= index)
		return children[index];
	return nullptr;
}

GameObject * Transform::GetGameObject()
{
	return gameObject;
}

void Transform::RemoveChildAt(int index)
{
	if (children.size() >= index)
	{
		children.at(index)->RevertLocalMatrix();
		children.erase(children.begin() + index);
	}
}

void Transform::RemoveChild(Transform * child)
{
	children.erase(find(children.begin(), children.end(), child));
}

void Transform::DecomposeWorldMAtrix()
{
	if (parentTransform != nullptr)
	{
		glm::vec3 pos;
		glm::vec3 scale;
		glm::quat rotation;

		glm::vec3 skew;
		glm::vec4 persp;

		glm::decompose(worldMatrix, scale, rotation, pos, skew, persp);

		worldRotation = glm::degrees(glm::eulerAngles(rotation).z);
		worldPosition = Vector<glm::vec2>(glm::ceil(pos.x), glm::ceil(pos.y));
		worldScale = Vector<glm::vec2>(glm::roundEven(scale.x), glm::roundEven(scale.y));
	}
	else
	{
		worldRotation = localRotation;
		worldPosition = LocalPosition();
		worldScale = localScale;
	}
}

void Transform::RevertLocalMatrix()
{
	parentTransform = nullptr;

	if (cameraFix == -1)
		ResetCameraScale();
	else
		SetScale(worldScale);

	SetPosition(worldPosition);
	SetRotation(worldRotation);
}

void Transform::CameraUpdatedSize()
{
	CheckForUpdate();
}

void Transform::SetLayer(short layer)
{
	if (cameraFix != -1)
	{
		if (layer != translationMatrix[3][2] - 999)
		{
			translationMatrix[3][2] = layer - 999;
			CheckForUpdate();
		}
	}
}

short Transform::GetLayer() const
{
	return translationMatrix[3][2];
}

void Transform::CheckForUpdate()
{
	if (!haveToUpdate)
	{
		if (gameObject != nullptr)
			Renderer::TransformUpdates.push_back(gameObject);
		haveToUpdate = true;
	}

	if (children.size() > 0)
	{
		for (Transform* c : children)
			c->CheckForUpdate();
	}
}

void Transform::ResetUpdateStatus()
{
	haveToUpdate = false;
}

bool Transform::UpdateStatus()
{
	return haveToUpdate;
}

void Transform::UpdateScaleCorrection()
{
	scaleCorrection = (&ApplicationData::GetInstance())->aspectScale;
}