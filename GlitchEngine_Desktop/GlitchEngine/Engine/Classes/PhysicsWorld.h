#pragma once
#include <Box2D/Box2D.h>
#include <glm/glm.hpp>

#include <Utils/Vector.h>

class PhysicsWorld
{
private:
	b2World *world;
	b2Vec2 gravity;
	int velocityIterations;
	int positionIterations;
	float timeStep;
	float simulationUnits;
	bool debugDraw;

public:

	static PhysicsWorld &GetInstance();
	void Destroy();

	PhysicsWorld();
	~PhysicsWorld();
	void SetGravity(Vector<glm::vec2> newGravity);
	void SetSimulationUnitConversion(float ratio);

	b2World *GetWorld();
	void Step();
	float GetSimulationUnits();

	bool GetDebugDraw() const;
	void SetDebugDraw(bool d);

};