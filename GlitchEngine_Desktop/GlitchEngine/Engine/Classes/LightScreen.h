#pragma once

#include <Objects/Camera.h>
#include <Utils/ShaderLoader.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>


class LightScreen
{

public:

	struct LightSource
	{
		bool active;
		Vector<glm::vec2> position;
		Vector<glm::vec3> color;
		float brightness;
		float radious;

		bool positionNeedsUpdate;
		bool colorNeedsUpdate;
		bool brightnessNeedsUpdate;
		bool radiousNeedsUpdate;

		LightSource(Vector<glm::vec2> p, Vector<glm::vec3> c, float b, float r, bool a)
		{
			position = p;
			color = c;
			brightness = b;
			radious = r;
			active = a;
			(&LightScreen::GetInstance())->AddToLightSources(this);
		}

		Vector<glm::vec2> GetPosition() const { return position; }
		void SetPosition(Vector<glm::vec2> pos) { position = pos; positionNeedsUpdate = true; }

		Vector<glm::vec3> GetColor() const { return color; }
		void SetColor(Vector<glm::vec3> c) { color = c; colorNeedsUpdate = true; }

		float GetBrightness() const { return brightness; }
		void SetBrightness(float b) { brightness = b; brightnessNeedsUpdate = true; }

		float GetRadious() const { return radious; }
		void SetRadious(float r) { radious = r; radiousNeedsUpdate = true; }

		bool GetActive() const { return active; }
		void SetActive(bool a) { active = a; }

		void ResetUpdateStatus()
		{
			positionNeedsUpdate = false;
			colorNeedsUpdate = false;
			brightnessNeedsUpdate = false;
			radiousNeedsUpdate = false;
		}
	};

	LightScreen();
	~LightScreen();
	static LightScreen &GetInstance();
	void Destroy();

	void AddToLightSources(LightSource* source);
	void DrawScreen(Camera* cam);
	void SetAmbientColor(Vector<glm::vec3> color);

private:
	ApplicationData* appData;
	GLuint vertexBuffer;
	GLuint vertexArray;
	std::vector<GLfloat> quadVerts;
	std::vector<LightSource*> lightSources;

	GLuint shaderID;
	Transform transform;
	glm::mat4 modelMatrix;
	std::map<string, GLuint> uniformLocations;
	int lastNumberOfLights;
	bool PointInsideScreen(Vector<glm::vec2>, Vector<glm::vec2> camPos);
};
