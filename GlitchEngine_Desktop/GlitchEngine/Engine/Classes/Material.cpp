#include <Classes/Material.h>
#include <Classes/Renderer.h>

Material::Material(string shader, bool _translucent)
{
	vertexArrayPosition = -1;
	translucent = _translucent;
	haveToUpdate = false;
	isInitialized = false;
	this->shader = shader;
	RM = &ResourceManager::GetInstance();

	if (shader != "GE_Font" && shader != "GE_Movie")
		InitializeShader(false);
}

Material::Material()
{
	vertexArrayPosition = -1;
	haveToUpdate = false;
	isInitialized = false;
	translucent = false;
	shader = "GE_Color";
	SetVec4("Color", Vector<glm::vec4>(1, 1, 1, 1));
	RM = &ResourceManager::GetInstance();
	InitializeShader(false);
	CheckForUpdate();
}

Material::~Material()
{

}

GLuint Material::GetShader()
{
	return ShaderLoader::GetProgramIDAt(shader);
}

GLuint Material::GetArrayPosition()
{
	return (GLuint)vertexArrayPosition;
}

void Material::SetInt(string key, int value)
{
	intMap[key] = value;
	CheckForUpdate();
}

void Material::SetFloat(string key, float value)
{
	floatMap[key] = value;
	CheckForUpdate();
}

void Material::SetVec2(string key, Vector<glm::vec2> value)
{
	vec2Map[key] = value.GetVec();
	CheckForUpdate();
}

void Material::SetVec3(string key, Vector<glm::vec3> value)
{
	vec3Map[key] = value.GetVec();
	CheckForUpdate();
}

void Material::SetVec4(string key, Vector<glm::vec4> value)
{
	vec4Map[key] = value.GetVec();
	CheckForUpdate();
}

void Material::SetTexture(string key, string fileName) {

	std::vector<std::string> keys;
	std::stringstream ss(key);
	std::string keyPart;

	while (getline(ss, keyPart, ',')) {
		keys.push_back(keyPart);
	}

	std::vector<std::string> fileNames;
	ss = std::stringstream(fileName);
	std::string fileNamePart;

	while (getline(ss, fileNamePart, ',')) {
		fileNames.push_back(fileNamePart);
	}

	uniqueTextureCombID = 0;
	for (unsigned int i = 0; i < keys.size(); i++)
	{
		auto it = RM->textures.find(fileNames[i]);
		if (it != RM->textures.end())
		{
			textures[keys[i]] = it->second.bufferID;
			uniqueTextureCombID += it->second.bufferID;
		}
		else
			Logger::Log("Warning!: Texture %s not found.", fileNames[i].c_str());
	}

	if (!isInitialized)
		InitializeShader(true);
}

void Material::SetMovieTextures(GLuint yBuffer, GLuint uBuffer, GLuint vBuffer)
{
	textures["planeY"] = yBuffer;
	textures["planeU"] = uBuffer;
	textures["planeV"] = vBuffer;
	uniqueTextureCombID = yBuffer + uBuffer + vBuffer;

	if (!isInitialized)
		InitializeShader(true);
}

void Material::SetFontTexture(string fontName, FT_Face face, unsigned width, unsigned height)
{
	glGenTextures(1, &fontTextureID);
	glBindTexture(GL_TEXTURE_2D, fontTextureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, 0);

	int xOffSet = 0;
	for (int i = 32; i < 128; i++)
	{
		FT_Load_Char(face, i, FT_LOAD_RENDER);
		glTexSubImage2D(GL_TEXTURE_2D, 0, xOffSet, 0, face->glyph->bitmap.width, face->glyph->bitmap.rows, GL_ALPHA, GL_UNSIGNED_BYTE, face->glyph->bitmap.buffer);
		xOffSet += face->glyph->bitmap.width;
	}
}

GLuint Material::GetFontTextureID()
{
	return fontTextureID;
}

void Material::UpdateBuffers()
{
	for (auto it : intMap)
	{
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		for (int i = 0; i < parentsPositions.size(); i++)
			glBufferSubData(GL_ARRAY_BUFFER, parentsPositions[i] * sizeof(int), sizeof(int), &it.second);
	}

	for (auto it : floatMap)
	{
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		for (int i = 0; i < parentsPositions.size(); i++)
			glBufferSubData(GL_ARRAY_BUFFER, parentsPositions[i] * sizeof(float), sizeof(float), &it.second);
	}

	for (auto it : vec2Map)
	{
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		for (int i = 0; i < parentsPositions.size(); i++)
			glBufferSubData(GL_ARRAY_BUFFER, parentsPositions[i] * sizeof(glm::vec2), sizeof(glm::vec2), glm::value_ptr(it.second));
	}


	for (auto it : vec3Map)
	{
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		for (int i = 0; i < parentsPositions.size(); i++)
			glBufferSubData(GL_ARRAY_BUFFER, parentsPositions[i] * sizeof(glm::vec3), sizeof(glm::vec3), glm::value_ptr(it.second));
	}

	for (auto it : vec4Map)
	{
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		for (int i = 0; i < parentsPositions.size(); i++)
			glBufferSubData(GL_ARRAY_BUFFER, parentsPositions[i] * sizeof(glm::vec4), sizeof(glm::vec4), glm::value_ptr(it.second));
	}

	haveToUpdate = false;
	ClearMaps();
}

void Material::CheckForUpdate()
{
	if (parentsPositions.size() > 0 && !haveToUpdate)
	{
		Renderer::MaterialUpdates.push_back(this);
		haveToUpdate = true;
	}
}

void Material::AddParentPosition(int pos)
{
	parentsPositions.push_back(pos);
	CheckForUpdate();
}

void Material::ClearMaps()
{
	intMap.clear();
	floatMap.clear();
	vec2Map.clear();
	vec3Map.clear();
	vec4Map.clear();
	mat4Map.clear();
	textures.clear();
}

void Material::InitializeShader(bool fromTextureFunction)
{
	GLuint* vertexArrayID;
	GLuint shaderID = ShaderLoader::GetProgramIDAt(shader);
	map<string, string> attributes = ShaderLoader::GetAttributes(shader);
	int translucentMod = translucent ? 2500 : 0;

	if (!fromTextureFunction)
	{
		if (Renderer::ShaderVertexRelation[shaderID].size() > 0)
			for (auto it : Renderer::ShaderVertexRelation[shaderID])
			{
				vertexArrayPosition = it;
				return;
			}

		if (attributes.find("tex") != attributes.end())
			return;
	}
	else
	{
		if (Renderer::ShaderTextureVertexRelation[shaderID].size() > 0)
		{
			auto it = Renderer::ShaderTextureVertexRelation[shaderID].find(uniqueTextureCombID);
			if (it != Renderer::ShaderTextureVertexRelation[shaderID].end())
			{
				vertexArrayPosition = it->second;
				return;
			}
		}
	}

	vertexArrayPosition = Renderer::VertexArraysID.size() + translucentMod;
	vertexArrayID = &Renderer::VertexArraysID[vertexArrayPosition];

	if (fromTextureFunction)
	{
		Renderer::VertexTexturesRelation[vertexArrayPosition] = textures;
		Renderer::ShaderTextureVertexRelation[shaderID][uniqueTextureCombID] = vertexArrayPosition;
	}

	glGenVertexArrays(1, vertexArrayID);
	glBindVertexArray(*vertexArrayID);

	Renderer::ShaderVertexRelation[shaderID].push_back(vertexArrayPosition);
	Renderer::VertexShaderRelation[vertexArrayPosition] = shaderID;

	glBindBuffer(GL_ARRAY_BUFFER, ShaderLoader::VertexBufferID);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), 0);

	int pos = glGetAttribLocation(shaderID, "textureCoords");
	if (pos != -1)
	{
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	}

	glGenBuffers(1, &Renderer::RenderableBuffers[vertexArrayPosition]["Model"]);
	glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition]["Model"]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * 100000, NULL, GL_STREAM_DRAW);

	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	glEnableVertexAttribArray(4);
	glEnableVertexAttribArray(5);

	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), 0);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(sizeof(glm::vec4)));
	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(2 * sizeof(glm::vec4)));
	glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(3 * sizeof(glm::vec4)));

	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);
	glVertexAttribDivisor(4, 1);
	glVertexAttribDivisor(5, 1);

	for (auto const it : attributes)
	{
		int pos = glGetAttribLocation(shaderID, it.first.c_str());
		if (pos == -1)
		{
			if (it.first != "tex")
				Logger::Log("Error!: Shader <%s> has no attribute %s.", shader.c_str(), it.first.c_str());
			continue;
		}

		glGenBuffers(1, &Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		glBindBuffer(GL_ARRAY_BUFFER, Renderer::RenderableBuffers[vertexArrayPosition][it.first.c_str()]);
		glEnableVertexAttribArray(pos);
		glVertexAttribDivisor(pos, 1);

		if (it.second == "int")
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(int) * 100000, NULL, GL_STREAM_DRAW);
			glVertexAttribPointer(pos, 1, GL_INT, GL_FALSE, sizeof(int), 0);
		}

		if (it.second == "float")
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 100000, NULL, GL_STREAM_DRAW);
			glVertexAttribPointer(pos, 1, GL_FLOAT, GL_FALSE, sizeof(float), 0);
		}

		if (it.second == "vec2")
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * 100000, NULL, GL_STREAM_DRAW);
			glVertexAttribPointer(pos, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);
		}

		if (it.second == "vec3")
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 100000, NULL, GL_STREAM_DRAW);
			glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), 0);
		}

		if (it.second == "vec4")
		{
			glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * 100000, NULL, GL_STREAM_DRAW);
			glVertexAttribPointer(pos, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec4), 0);
		}
	}

	glBindVertexArray(0);
	isInitialized = true;
}