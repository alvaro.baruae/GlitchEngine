#pragma once
#include <map>
#include <vector>
#include <algorithm>
#include <Objects/Camera.h>
#include <Objects/MovieQuad.h>
#include <Objects/GameObject.h>

using namespace std;

struct TextureInstancedInfo
{
	int index;
	int size;
	vector<GLuint> textureIDs;
};

class Renderer
{
public:

	static int ObjectUniqueID;

	static map<GLuint, GLuint> VertexArraysID;
	static map<GLuint, int> RenderableObjects;
	static map<GLuint, vector<int>> AvailableSpaces;
	static map<GLuint, map<string, GLuint>> RenderableBuffers;
	static map<GLuint, vector<int>> ShaderVertexRelation;
	static map<GLuint, GLuint> VertexShaderRelation;
	static map<GLuint, map<GLuint, GLuint>> ShaderTextureVertexRelation;
	static map<GLuint, map<string, GLuint>> VertexTexturesRelation;
	static vector<Material*> MaterialUpdates;
	static vector<GameObject*> ObjectsWithRigidBody;
	static vector<GameObject*> TransformUpdates;
	static vector<GameObject*> ComponentsToAddAfterUpdate;
	static vector<GameObject*> ComponentsToDeleteAfterUpdate;
	static vector<GameObject*> ComponentsUpdate;

	static void DrawAllObjects(Camera* cam)
	{
		for (auto const tUpdates : TransformUpdates)
			tUpdates->SendModel();
		TransformUpdates.clear();

		for (auto const mUpdates : MaterialUpdates)
			mUpdates->UpdateBuffers();
		MaterialUpdates.clear();

		if (cam->HaveToUpdate())
		{
			glm::mat4 viewProj = cam->GetProjectionMatrix() * cam->GetViewMatrix();
			for (auto const it : ShaderLoader::ProgramIDs)
			{
				if (ShaderLoader::GetCameraUniformLocation(it.second) != -1)
				{
					glUseProgram(it.second);
					glUniformMatrix4fv(ShaderLoader::GetCameraUniformLocation(it.second), 1, GL_FALSE, glm::value_ptr(viewProj));
					glUseProgram(0);
				}
			}

			if (!(&ApplicationData::GetInstance())->useLights)
				cam->GetTransform()->ResetUpdateStatus();
		}
		
		// Solid Queue
		for (auto const it : VertexArraysID)
		{
			if (it.first < 2500)
			{
				glBindVertexArray(it.second);
				glUseProgram(VertexShaderRelation[it.first]);

				auto it2 = VertexTexturesRelation.find(it.first);
				if (it2 != VertexTexturesRelation.end())
				{
					unsigned int k = 0;
					for (auto it3 : VertexTexturesRelation[it.first])
					{
						glActiveTexture(GL_TEXTURE0 + k);
						glBindTexture(GL_TEXTURE_2D, it3.second);
						glUniform1i(glGetUniformLocation(VertexShaderRelation[it.first], it3.first.c_str()), k);
						k++;
					}
				}

				glDrawArraysInstanced(GL_QUADS, 0, 4, RenderableObjects[it.first]);
				glBindVertexArray(0);
				glUseProgram(0);
			}
		}

		//Transparent Queue
		for (auto const it : VertexArraysID)
		{
			if (it.first >= 2500)
			{
				glBindVertexArray(it.second);
				glUseProgram(VertexShaderRelation[it.first]);

				auto it2 = VertexTexturesRelation.find(it.first);
				if (it2 != VertexTexturesRelation.end())
				{
					unsigned int k = 0;
					for (auto it3 : VertexTexturesRelation[it.first])
					{
						glActiveTexture(GL_TEXTURE0 + k);
						glBindTexture(GL_TEXTURE_2D, it3.second);
						glUniform1i(glGetUniformLocation(VertexShaderRelation[it.first], it3.first.c_str()), k);
						k++;
					}
				}

				glDrawArraysInstanced(GL_QUADS, 0, 4, RenderableObjects[it.first]);
				glBindVertexArray(0);
				glUseProgram(0);
			}
		}
	}

	static void UpdateRigidbodies()
	{
		for (auto const it : ObjectsWithRigidBody)
			if (it->GetActiveState() && it->GetRigidBody() != nullptr && it->GetRigidBody()->GetActiveState())
				it->GetRigidBody()->Update();
	}

	static void DeleteBuffers()
	{
		for (auto const it : RenderableBuffers)
			for (auto const it2 : it.second)
				glDeleteBuffers(1, &it2.second);

		for (auto const it : VertexArraysID)
			glDeleteVertexArrays(1, &it.second);
	}
};