#include <Classes/SpriteAnimator.h>


SpriteAnimator::SpriteAnimator()
{
	RM = &ResourceManager::GetInstance();
	LED = &LuaEventDispatcher::GetInstance();
}

SpriteAnimator::~SpriteAnimator()
{

}

SpriteAnimator &SpriteAnimator::GetInstance()
{
	static SpriteAnimator *instance;
	if (instance == NULL)
		instance = new SpriteAnimator();
	return *instance;

}

void SpriteAnimator::Destroy()
{
	SpriteAnimator *ins = &GetInstance();
	if (ins != NULL)
		delete ins;
}

void SpriteAnimator::UpdateAnimations(double deltaTime)
{
	for (unsigned int i = 0; i < animationData.size(); i++)
	{
		AnimationInfo animInfo = animationData[i];
		if ((animInfo.state == AnimationState::Playing || animInfo.state == AnimationState::Looping) && animInfo.currentAnimation != "None")
		{
			animInfo.animDeltaCounter += deltaTime;
			if (animInfo.animDeltaCounter > animInfo.animationSpeed)
			{
				Quad* quad = animInfo.parent;
				vector<string> animation = animInfo.animations[animInfo.currentAnimation];

				animInfo.currentFrame++;
				if (animInfo.currentFrame >= animation.size())
				{
					animInfo.currentFrame = 0;
					if (animInfo.state == AnimationState::Playing)
						animInfo.state = AnimationState::Stoped;
				}

				for (pair<int, int> it : animInfo.frameEvents[animInfo.currentAnimation])
					if (it.first == animInfo.currentFrame)
					{
						LED->QueueLuaEvent(it.second);
						break;
					}

				FrameInfo fInfo = frameData[i][animation[animInfo.currentFrame]];
				quad->GetMaterial()->SetVec4("frame", Vector<glm::vec4>(fInfo.x, fInfo.y, fInfo.w, fInfo.h));
				quad->GetTransform()->SetScale(Vector<glm::vec2>(fInfo.w, fInfo.h) * animInfo.spriteScale);

				animInfo.animDeltaCounter = 0;
			}
			animationData[i] = animInfo;
		}
	}
}

void SpriteAnimator::RegisterAnimation(int index, string animName, string prefix, int startingIndex, int endIndex, int padding, string suffix)
{
	std::vector<std::string> parts;

	if (startingIndex == 0 && endIndex == 0)
		animationData[index].animations[animName].push_back(animName.c_str());
	else
	{
		for (int i = startingIndex; i <= endIndex; i++)
		{
			string number = to_string(i);
			string padString = std::string(padding - number.length(), '0') + number;
			string partName = prefix + padString + suffix;
			parts.push_back(partName);
		}

		for (unsigned int i = 0; i < parts.size(); i++)
			animationData[index].animations[animName].push_back(parts[i].c_str());
	}
}

void SpriteAnimator::RegisterFrameEvent(int index, string animName, int frameIndex)
{
	animationData[index].frameEvents[animName].push_back(make_pair(frameIndex - 1, LED->GetLuaFunctionReference()));
}

int SpriteAnimator::RegisterQuad(Quad* quad, string jsonAnimFile)
{
	int index = LoadJsonAnimFile(RM->animJsonFileLocations[jsonAnimFile].c_str());
	animationData[index].parent = quad;
	quad->GetMaterial()->SetVec2("texel", Vector<glm::vec2>(animationData[index].texelSizeX, animationData[index].texelSizeY));
	return index;
}

void SpriteAnimator::SetAnimationSpeed(int index, int framesPerSecond)
{
	animationData[index].animationSpeed = 1.0 / (double)framesPerSecond * 1000000000;
}

int SpriteAnimator::LoadJsonAnimFile(const char* filePath)
{
	Json::Value root;
	std::ifstream animFile(filePath, std::ifstream::binary);
	animFile >> root;

	AnimationInfo animInfo;
	animInfo.spriteH = root["meta"]["height"].asInt();
	animInfo.spriteW = root["meta"]["width"].asInt();
	animInfo.texelSizeX = 1.0f / (float)animInfo.spriteW;
	animInfo.texelSizeY = 1.0f / (float)animInfo.spriteH;
	animInfo.animationSpeed = 1.0 / 30.0 * 1000000000;
	animInfo.currentAnimation = "None";
	animInfo.animDeltaCounter = 0.0f;
	animInfo.spriteScale = Vector<glm::vec2>(1.0f, 1.0f);
	animInfo.currentFrame = 0;
	animInfo.state = AnimationState::Paused;

	int index = animationData.size();
	animationData.push_back(animInfo);

	const Json::Value frameInfo = root["frames"];
	for (const auto &it : frameInfo)
	{
		FrameInfo fInfo;
		fInfo.x = it["x"].asInt();
		fInfo.y = it["y"].asInt();
		fInfo.h = it["h"].asInt();
		fInfo.w = it["w"].asInt();
		frameData[index][it["name"].asString()] = fInfo;
	}

	return index;
}

void SpriteAnimator::PlayAnimation(int index, string animationName, bool loop)
{
	animationData[index].currentAnimation = animationName;
	if (loop)
		animationData[index].state = AnimationState::Looping;
	else
		animationData[index].state = AnimationState::Playing;

	vector<string> animation = animationData[index].animations[animationData[index].currentAnimation];
	FrameInfo fInfo = frameData[index][animation[0]];
	animationData[index].parent->GetMaterial()->SetVec4("frame", Vector<glm::vec4>(fInfo.x, fInfo.y, fInfo.w, fInfo.h));
	animationData[index].parent->GetTransform()->SetScale(Vector<glm::vec2>(fInfo.w, fInfo.h) * animationData[index].spriteScale);
}

void SpriteAnimator::PauseAnimation(int index)
{
	animationData[index].state = AnimationState::Paused;
}

void SpriteAnimator::ResumeAnimation(int index, bool loop)
{
	if (loop)
		animationData[index].state = AnimationState::Looping;
	else
		animationData[index].state = AnimationState::Playing;
}

void SpriteAnimator::StopAnimation(int index)
{
	animationData[index].currentFrame = 0;
	animationData[index].state = AnimationState::Stoped;
}

void SpriteAnimator::SetAnimationScale(int index, Vector<glm::vec2> scale)
{
	animationData[index].spriteScale = scale;
}