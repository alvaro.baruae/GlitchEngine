#pragma once

#include <Objects/Quad.h>
#include <json/json.h>
#include <map>
#include <vector>
#include <fstream>
#include <glm/glm.hpp>
#include <Utils/LuaEventDispatcher.h>
#include <Utils/ResourceManager.h>

enum AnimationState
{
	Playing = 0,
	Stoped = 1,
	Paused = 2,
	Looping = 4,
};

struct AnimationInfo
{
	Quad* parent;
	int spriteW;
	int spriteH;
	float texelSizeX;
	float texelSizeY;
	long long animationSpeed;
	Vector<glm::vec2> spriteScale;
	string currentAnimation;
	unsigned int currentFrame;
	double animDeltaCounter;
	AnimationState state;
	map<string, vector<string>> animations;
	map<string, vector<pair<int,int>>> frameEvents;
};

struct FrameInfo
{
	int x;
	int y;
	int w;
	int h;
};

class SpriteAnimator
{
private:
	ResourceManager* RM;
	LuaEventDispatcher *LED;

	map<int, map<string, FrameInfo>> frameData;
	vector<AnimationInfo> animationData;
	int LoadJsonAnimFile(const char* filePath);
	

public:
	SpriteAnimator();
	~SpriteAnimator();

	static SpriteAnimator &GetInstance();
	void Destroy();

	int RegisterQuad(Quad* mat, string jsonAnimFile);
	void RegisterAnimation(int index, string animName, string prefix, int startingIndex, int endIndex, int padding, string suffix);
	void RegisterFrameEvent(int index, string animName, int frameIndex);
	void SetAnimationSpeed(int index, int framesPerSecond);
	void SetAnimationScale(int index, Vector<glm::vec2> scale);
	void PlayAnimation(int index, string animationName, bool loop);
	void PauseAnimation(int index);
	void ResumeAnimation(int index, bool loop);
	void StopAnimation(int index);

	void UpdateAnimations(double deltaTime);
};

