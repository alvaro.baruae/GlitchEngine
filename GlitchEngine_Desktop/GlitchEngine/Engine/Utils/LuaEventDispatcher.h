#pragma once
#define lua_PrintError(state, section) { luaL_traceback(state, state, section, 1); Logger::Log("%s", lua_tostring(state, -1)); lua_pop(state, 1); Logger::Log("%s", lua_tostring(state, -1)); }

#include <map>
#include <vector>
#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>
#include <Utils/Logger.h>

struct CollisionInfo;
class b2Contact;
class LuaEventDispatcher
{
private:
	lua_State* L;
	std::map<int, CollisionInfo> collisionFunctionReferences;
	std::vector<int> functionReferences;
	std::vector<int> offFunctionReferences;
	int tableIndex;

public:
	LuaEventDispatcher();
	~LuaEventDispatcher();
	static LuaEventDispatcher &GetInstance();
	void SetLuaState(lua_State *L);
	lua_State* GetLuaState();

	void QueueLuaCollisionEvent(int index, CollisionInfo data);
	void CallPreSolveEvent(int index, CollisionInfo data, b2Contact* contact);
	void QueueLuaEvent(int index);
	void TriggerLuaEvent();
	int GetLuaFunctionReference();

	void Dispatch();
	void DispatchLoadingQueueEvents(int index, float progress);
	void Destroy();
};