#include <Utils/Application.h>


ApplicationData::ApplicationData()
{
}


ApplicationData::~ApplicationData()
{
}

ApplicationData &ApplicationData::GetInstance()
{
	static ApplicationData* ins;
	if (ins == NULL)
		ins = new ApplicationData();
	return *ins;
}

void ApplicationData::Destroy()
{
	delete &GetInstance();
}