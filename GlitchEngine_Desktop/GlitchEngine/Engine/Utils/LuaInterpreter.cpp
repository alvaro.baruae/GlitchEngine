#include <Utils/LuaInterpreter.h>

LuaInterpreter::LuaInterpreter()
{
	endExecution = false;
	timeHandle = &TimeHandle::GetInstance();
	RM = &ResourceManager::GetInstance();
	LED = &LuaEventDispatcher::GetInstance();
	gui = &Gui::GetInstance();
	appData = &ApplicationData::GetInstance();

	luaState = luaL_newstate();
	luaL_openlibs(luaState);
	LED->SetLuaState(luaState);

	RegisterConstants();
	RegisterClasses();

	luaL_dostring(luaState, "print = function(text) Logger.Log(text) end");
	luaL_dostring(luaState, "Time = Time.GetInstance()");
	luaL_dostring(luaState, "GUI = GUI.GetInstance()");
	luaL_dostring(luaState, "Input = Input.GetInstance()");
	luaL_dostring(luaState, "SpriteAnimator = SpriteAnimator.GetInstance()");
	luaL_dostring(luaState, "PhysicsWorld = PhysicsWorld.GetInstance()");
	luaL_dostring(luaState, "TiledMapParser = TiledMapParser.GetInstance()");
	luaL_dostring(luaState, "LightScreen = LightScreen.GetInstance()");
	luaL_dostring(luaState, "Resources = Resources.GetInstance()");
	luaL_dostring(luaState, "PostProcessing = PostProcessing.GetInstance()");
	luaL_dostring(luaState, "GE_GameObjects = {}");
	luaL_dostring(luaState, "function Destroy(object) if object.__gc ~= nil then object:__gc() end end");
	luaL_dostring(luaState, "function AddComponent(gameObject, componentName) local guid = gameObject:GetUID() gameObject : RegisterComponentID(componentName) local object = GE_GameObjects[guid] if (object == nil) then GE_GameObjects[guid] = {} object = GE_GameObjects[guid] end object[componentName] = {} local component = object[componentName] debug.setupvalue(_G[componentName], 1, component) _G[componentName]() setmetatable(component, { __index = _G }) if component['GEC_Init'] ~= nil then component['GEC_Init'](gameObject) end if component['Start'] ~= nil then component['Start']() end end");
	luaL_dostring(luaState, "function GetComponent(gameObject, componentName) local component = GE_GameObjects[gameObject:GetUID()][componentName] return component end");
	luaL_dostring(luaState, "function EnableComponent(gameObject, componentName) local component = GetComponent(gameObject, componentName) if component['GEC_EnabledStatus']() == false then component['GEC_Enable']() if component['OnEnabled'] ~= nil then component['OnEnabled']() end end end");
	luaL_dostring(luaState, "function DisableComponent(gameObject, componentName) local component = GetComponent(gameObject, componentName) if component['GEC_EnabledStatus']() == true then component['GEC_Disable']() end end");
	luaL_dostring(luaState, "GE_WAITING_ON_TIME = {} GE_WAITING_ON_SIGNAL = {} GE_CURRENT_TIME = 0");
	luaL_dostring(luaState, "function StartCoroutine(func) local co = coroutine.create(func) coroutine.resume(co) end");
	luaL_dostring(luaState, "function WaitForSeconds(seconds) local co = coroutine.running() assert(co ~= nil, 'The main thread cannot wait!') local wakeupTime = GE_CURRENT_TIME + seconds GE_WAITING_ON_TIME[co] = wakeupTime return coroutine.yield(co) end");
	luaL_dostring(luaState, "function WaitForSignal(signalName) local co = coroutine.running() assert(co ~= nil, 'The main thread cannot wait!') if GE_WAITING_ON_SIGNAL[signalName] == nil then GE_WAITING_ON_SIGNAL[signalName] = { co } else table.insert(GE_WAITING_ON_SIGNAL[signalName], co) end return coroutine.yield(co) end");
	luaL_dostring(luaState, "function BroadcastSignal(signalName) local threads = GE_WAITING_ON_SIGNAL[signalName] if threads == nil then return end for _, co in ipairs(threads) do coroutine.resume(co) end GE_WAITING_ON_SIGNAL[signalName] = nil end");
	luaL_dostring(luaState, "function GE_UpdateCoroutines() GE_CURRENT_TIME = GE_CURRENT_TIME + Time.deltaTime local coroutinesToWake = {} for coroutine, wakeUpTime in pairs(GE_WAITING_ON_TIME) do if wakeUpTime < GE_CURRENT_TIME then table.insert(coroutinesToWake, coroutine) end end for _, co in ipairs(coroutinesToWake) do GE_WAITING_ON_TIME[co] = nil coroutine.resume(co) end end");
	luaL_dostring(luaState, "GE_Internals = { Update = function() GE_UpdateCoroutines() end }");

	for (auto const& it : RM->glitchComponents)
	{
		const char* fileName = it.first.c_str();
		string toAppend = "local gameObject = nil local enabled = nil function GEC_Init(GO) gameObject = GO enabled = true end function GEC_Disable() enabled = false end function GEC_EnabledStatus() return enabled end function GEC_Enable() enabled = true end ";
		FILE *f = fopen(fileName, "rt");
		fseek(f, 0, SEEK_END);
		int count = (int)ftell(f);
		rewind(f);
		char *data = (char*)malloc(sizeof(char)*(count + 1));
		count = (int)fread(data, sizeof(char), count, f);
		data[count] = '\0';
		fclose(f);
		string text = string(data);
		string wholeText = toAppend.append(text);
		if (luaL_loadstring(luaState, wholeText.c_str()))
		{
			lua_PrintError(luaState, fileName);
			return;
		}
		lua_setglobal(luaState, it.second.c_str());
	}

	for (auto const& it : RM->glitchPrefabs)
	{
		const char* fileName = it.c_str();
		if (luaL_dofile(luaState, fileName))
		{
			lua_PrintError(luaState, fileName);
			return;
		}
	}

	for (auto const& it : RM->luaFileNames)
	{
		const char* fileName = it.first.c_str();
		if (luaL_dofile(luaState, fileName))
		{
			lua_PrintError(luaState, it.second.c_str());
			return;
		}

		lua_getglobal(luaState, it.second.c_str());
		if (!lua_isnil(luaState, -1))
		{
			lua_getfield(luaState, -1, "Awake");
			if (!lua_isnil(luaState, -1))
				awakeFiles.push_back(it.second);	
			lua_pop(luaState, 1);

			lua_getfield(luaState, -1, "Update");
			if (!lua_isnil(luaState, -1))
				updateFiles.push_back(it.second);
			lua_pop(luaState, 1);

			lua_getfield(luaState, -1, "OnGUI");
			if (!lua_isnil(luaState, -1))
				onGuiFiles.push_back(it.second);
			lua_pop(luaState, 1);
		}
		lua_pop(luaState, 1);
	}

	for (auto const& it : awakeFiles)
	{
		lua_getglobal(luaState, it.c_str());
		lua_getfield(luaState, -1, "Awake");

		if (lua_pcall(luaState, 0, 0, 0))
		{
			lua_PrintError(luaState, it.c_str());
			return;
		}
		lua_pop(luaState, 1);
	}

	if (RM->luaFileNames.size() == 0)
	{
		Logger::Log("There is no game code.\nThe application will now terminate.");
		endExecution = true;
	}
}

LuaInterpreter::~LuaInterpreter()
{
	lua_close(luaState);
}

void LuaInterpreter::ProcessFiles()
{
	
	lua_getglobal(luaState, "GE_Internals");
	lua_getfield(luaState, -1, "Update");
	if (lua_pcall(luaState, 0, 0, 0))
	{
		lua_PrintError(luaState, "GE_Internals");
		return;
	}
	lua_pop(luaState, 1);

	for (auto const& it : updateFiles)
	{
		lua_getglobal(luaState, it.c_str());
		lua_getfield(luaState, -1, "enabled");

		bool enabled = true;
		if (!lua_isnil(luaState, -1))
			enabled = (bool)(lua_toboolean(luaState, -1));
		
		if (enabled)
		{
			lua_getfield(luaState, -2, "Update");
			if (lua_pcall(luaState, 0, 0, 0))
			{
				lua_PrintError(luaState, it.c_str());
				return;
			}
		}
		lua_pop(luaState, 2);
	}

	for (GameObject* it : Renderer::ComponentsUpdate)
	{
		if (it->GetActiveState())
		{
			lua_getglobal(luaState, "GE_GameObjects");
			if (!lua_isnil(luaState, -1))
			{
				lua_pushnumber(luaState, it->GetUID());
				lua_gettable(luaState, -2);
				if (lua_istable(luaState, -1))
				{
					for (string cIt : it->GetComponentIDs())
					{
						lua_pushstring(luaState, cIt.c_str());
						lua_gettable(luaState, -2);
						if (!lua_isnil(luaState, -1))
						{
							bool enabled = true;
							lua_getfield(luaState, -1, "GEC_EnabledStatus");
							if (!lua_isnil(luaState, -1))
							{
								lua_pcall(luaState, 0, 1, 0);
								if (lua_isboolean(luaState, -1))
									enabled = (bool)(lua_toboolean(luaState, -1));
							}

							if (enabled)
							{
								lua_getfield(luaState, -2, "Update");
								if (!lua_isnil(luaState, -1))
								{
									if (lua_pcall(luaState, 0, 0, 0))
									{
										lua_PrintError(luaState, cIt.c_str());
										return;
									}
								}
							}
							lua_pop(luaState, 1);
						}
						lua_pop(luaState, 1);
					}
				}
				lua_pop(luaState, 1);
			}
			lua_pop(luaState, 1);
		}
	}

	LED->Dispatch();

	if (Renderer::ComponentsToAddAfterUpdate.size() > 0)
	{
		for (auto const& it : Renderer::ComponentsToAddAfterUpdate)
			Renderer::ComponentsUpdate.push_back(it);
		Renderer::ComponentsToAddAfterUpdate.clear();
	}

	if (Renderer::ComponentsToDeleteAfterUpdate.size() > 0)
	{
		for (auto const& it : Renderer::ComponentsToDeleteAfterUpdate) 
		{
			vector<GameObject*>::iterator toDelete = find(Renderer::ComponentsUpdate.begin(), Renderer::ComponentsUpdate.end(), it);
			Renderer::ComponentsUpdate.erase(toDelete);
		}

		Renderer::ComponentsToDeleteAfterUpdate.clear();
	}

	lua_settop(luaState, 0);
}

void LuaInterpreter::ProcessGUI()
{
	glViewport((GLint)appData->baseSize.z, (GLint)appData->baseSize.w, (GLint)appData->baseSize.x, (GLint)appData->baseSize.y);
	gui->SetVertexArray();

	for (auto const& it : onGuiFiles)
	{
		lua_getglobal(luaState, it.c_str());
		lua_getfield(luaState, -1, "enabled");

		bool enabled = true;
		if (!lua_isnil(luaState, -1))
			enabled = (bool)(lua_toboolean(luaState, -1));

		if (enabled)
		{
			lua_getfield(luaState, -2, "OnGUI");
			if (lua_pcall(luaState, 0, 0, 0))
			{
				lua_PrintError(luaState, it.c_str());
				return;
			}
		}

		lua_pop(luaState, 2);
	}

	lua_settop(luaState, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

void LuaInterpreter::RegisterConstants()
{
	//KEYBOARD
	lua_SetConst(luaState, SDL_SCANCODE_A, "KEY_A");
	lua_SetConst(luaState, SDL_SCANCODE_B, "KEY_B");
	lua_SetConst(luaState, SDL_SCANCODE_C, "KEY_C");
	lua_SetConst(luaState, SDL_SCANCODE_D, "KEY_D");
	lua_SetConst(luaState, SDL_SCANCODE_E, "KEY_E");
	lua_SetConst(luaState, SDL_SCANCODE_F, "KEY_F");
	lua_SetConst(luaState, SDL_SCANCODE_G, "KEY_G");
	lua_SetConst(luaState, SDL_SCANCODE_H, "KEY_H");
	lua_SetConst(luaState, SDL_SCANCODE_I, "KEY_I");
	lua_SetConst(luaState, SDL_SCANCODE_J, "KEY_J");
	lua_SetConst(luaState, SDL_SCANCODE_K, "KEY_K");
	lua_SetConst(luaState, SDL_SCANCODE_L, "KEY_L");
	lua_SetConst(luaState, SDL_SCANCODE_M, "KEY_M");
	lua_SetConst(luaState, SDL_SCANCODE_N, "KEY_N");
	lua_SetConst(luaState, SDL_SCANCODE_O, "KEY_O");
	lua_SetConst(luaState, SDL_SCANCODE_P, "KEY_P");
	lua_SetConst(luaState, SDL_SCANCODE_Q, "KEY_Q");
	lua_SetConst(luaState, SDL_SCANCODE_R, "KEY_R");
	lua_SetConst(luaState, SDL_SCANCODE_S, "KEY_S");
	lua_SetConst(luaState, SDL_SCANCODE_T, "KEY_T");
	lua_SetConst(luaState, SDL_SCANCODE_U, "KEY_U");
	lua_SetConst(luaState, SDL_SCANCODE_V, "KEY_V");
	lua_SetConst(luaState, SDL_SCANCODE_W, "KEY_W");
	lua_SetConst(luaState, SDL_SCANCODE_X, "KEY_X");
	lua_SetConst(luaState, SDL_SCANCODE_Y, "KEY_Y");
	lua_SetConst(luaState, SDL_SCANCODE_Z, "KEY_Z");

	lua_SetConst(luaState, SDL_SCANCODE_0, "KEY_0");
	lua_SetConst(luaState, SDL_SCANCODE_1, "KEY_1");
	lua_SetConst(luaState, SDL_SCANCODE_2, "KEY_2");
	lua_SetConst(luaState, SDL_SCANCODE_3, "KEY_3");
	lua_SetConst(luaState, SDL_SCANCODE_4, "KEY_4");
	lua_SetConst(luaState, SDL_SCANCODE_5, "KEY_5");
	lua_SetConst(luaState, SDL_SCANCODE_6, "KEY_6");
	lua_SetConst(luaState, SDL_SCANCODE_7, "KEY_7");
	lua_SetConst(luaState, SDL_SCANCODE_8, "KEY_8");
	lua_SetConst(luaState, SDL_SCANCODE_9, "KEY_9");

	lua_SetConst(luaState, SDL_SCANCODE_F1, "KEY_F1");
	lua_SetConst(luaState, SDL_SCANCODE_F2, "KEY_F2");
	lua_SetConst(luaState, SDL_SCANCODE_F3, "KEY_F3");
	lua_SetConst(luaState, SDL_SCANCODE_F4, "KEY_F4");
	lua_SetConst(luaState, SDL_SCANCODE_F5, "KEY_F5");
	lua_SetConst(luaState, SDL_SCANCODE_F6, "KEY_F6");
	lua_SetConst(luaState, SDL_SCANCODE_F7, "KEY_F7");
	lua_SetConst(luaState, SDL_SCANCODE_F8, "KEY_F8");
	lua_SetConst(luaState, SDL_SCANCODE_F9, "KEY_F9");
	lua_SetConst(luaState, SDL_SCANCODE_F10, "KEY_F10");
	lua_SetConst(luaState, SDL_SCANCODE_F11, "KEY_F11");
	lua_SetConst(luaState, SDL_SCANCODE_F12, "KEY_F12");

	lua_SetConst(luaState, SDL_SCANCODE_UP, "KEY_UP");
	lua_SetConst(luaState, SDL_SCANCODE_DOWN, "KEY_DOWN");
	lua_SetConst(luaState, SDL_SCANCODE_LEFT, "KEY_LEFT");
	lua_SetConst(luaState, SDL_SCANCODE_RIGHT, "KEY_RIGHT");

	lua_SetConst(luaState, SDL_SCANCODE_LCTRL, "KEY_LCTRL");
	lua_SetConst(luaState, SDL_SCANCODE_RCTRL, "KEY_RCTRL");
	lua_SetConst(luaState, SDL_SCANCODE_RSHIFT, "KEY_RSHIFT");
	lua_SetConst(luaState, SDL_SCANCODE_LSHIFT, "KEY_LSHIFT");
	lua_SetConst(luaState, SDL_SCANCODE_LALT, "KEY_LALT");
	lua_SetConst(luaState, SDL_SCANCODE_RALT, "KEY_RALT");
	lua_SetConst(luaState, SDL_SCANCODE_SPACE, "KEY_SPACE");
	lua_SetConst(luaState, SDL_SCANCODE_ESCAPE, "KEY_ESCAPE");
	lua_SetConst(luaState, SDL_SCANCODE_RETURN, "KEY_ENTER");
	lua_SetConst(luaState, SDL_SCANCODE_BACKSPACE, "KEY_BACKSPACE");
	lua_SetConst(luaState, SDL_SCANCODE_TAB, "KEY_TAB");
	lua_SetConst(luaState, SDL_SCANCODE_DELETE, "KEY_DELETE");

	//MOUSE
	lua_SetConst(luaState, SDL_BUTTON_LEFT, "MOUSE_LEFT");
	lua_SetConst(luaState, SDL_BUTTON_MIDDLE, "MOUSE_MIDDLE");
	lua_SetConst(luaState, SDL_BUTTON_RIGHT, "MOUSE_RIGHT");

	//XBOX 360 Controller
	lua_SetConst(luaState, 0, "X360_DPADUP");
	lua_SetConst(luaState, 1, "X360_DPADDOWN");
	lua_SetConst(luaState, 2, "X360_DPADLEFT");
	lua_SetConst(luaState, 3, "X360_DPADRIGHT");
	lua_SetConst(luaState, 4, "X360_START");
	lua_SetConst(luaState, 5, "X360_BACK");
	lua_SetConst(luaState, 6, "X360_LSTICK");
	lua_SetConst(luaState, 7, "X360_RSTICK");
	lua_SetConst(luaState, 8, "X360_LS");
	lua_SetConst(luaState, 9, "X360_RS");
	lua_SetConst(luaState, 10, "X360_A");
	lua_SetConst(luaState, 11, "X360_B");
	lua_SetConst(luaState, 12, "X360_X");
	lua_SetConst(luaState, 13, "X360_Y");

	lua_SetConst(luaState, 0, "X360_LSTICKH");
	lua_SetConst(luaState, 1, "X360_LSTICKV");
	lua_SetConst(luaState, 2, "X360_RSTICKH");
	lua_SetConst(luaState, 3, "X360_RSTICKV");
	lua_SetConst(luaState, 4, "X360_LTRIGGER");
	lua_SetConst(luaState, 5, "X360_RTRIGGER");

	//TRANSFORM CONSTANTS
	lua_SetConst(luaState, Space::WorldSpace, "WORLDSPACE");
	lua_SetConst(luaState, Space::SelfSpace, "SELFSPACE");

	//CAMERA SPLIT SCREEN
	lua_SetConst(luaState, SplitType::TOP, "TOP");
	lua_SetConst(luaState, SplitType::BOTTOM, "BOTTOM");
	lua_SetConst(luaState, SplitType::LEFT, "LEFT");
	lua_SetConst(luaState, SplitType::RIGHT, "RIGHT");
	lua_SetConst(luaState, SplitType::TOP_RIGHT, "TOP_RIGHT");
	lua_SetConst(luaState, SplitType::TOP_LEFT, "TOP_LEFT");
	lua_SetConst(luaState, SplitType::BOTTOM_RIGHT, "BOTTOM_RIGHT");
	lua_SetConst(luaState, SplitType::BOTTOM_LEFT, "BOTTOM_LEFT");

	//WRAP
	lua_SetConst(luaState, GL_CLAMP_TO_EDGE, "CLAMP_WRAP");
	lua_SetConst(luaState, GL_REPEAT, "REPEAT_WRAP");

	//FILTER
	lua_SetConst(luaState, GL_LINEAR, "LINEAR_FILTER");
	lua_SetConst(luaState, GL_NEAREST, "POINT_FILTER");

	//PHYSICS
	lua_SetConst(luaState, b2BodyType::b2_dynamicBody, "DYNAMIC_BODY");
	lua_SetConst(luaState, b2BodyType::b2_kinematicBody, "KINEMATIC_BODY");
	lua_SetConst(luaState, b2BodyType::b2_staticBody, "STATIC_BODY");

	//FSMOperation
	lua_SetConst(luaState, FSMOperations::EQ, "FSMO_EQ");
	lua_SetConst(luaState, FSMOperations::NEQ, "FSMO_NEQ");
	lua_SetConst(luaState, FSMOperations::LEQ, "FSMO_LEQ");
	lua_SetConst(luaState, FSMOperations::GEQ, "FSMO_GEQ");
	lua_SetConst(luaState, FSMOperations::L, "FSMO_L");
	lua_SetConst(luaState, FSMOperations::G, "FSMO_G");
}

void LuaInterpreter::RegisterClasses()
{
	luabridge::getGlobalNamespace(luaState)
		.beginClass<Logger>("Logger")
		.addStaticFunction("Log", &Logger::SimplePrint)
		.endClass()
		.beginClass<TimeHandle>("Time")
		.addStaticFunction("GetInstance", &TimeHandle::GetInstance)
		.addProperty("deltaTime", &TimeHandle::GetDelta)
		.addProperty("time", &TimeHandle::GetTime)
		.endClass()
		.beginClass<GameObject>("GameObject")
		.addConstructor<void(*) ()>()
		.addFunction("GetTransform", &GameObject::GetTransform)
		.addFunction("GetRigidBody", &GameObject::GetRigidBody)
		.addFunction("AddRigidBody", &GameObject::AddRigidBody)
		.addFunction("GetUID", &GameObject::GetUID)
		.addFunction("RegisterComponentID", &GameObject::RegisterComponentID)
		.addProperty("active", &GameObject::GetActiveState, &GameObject::SetActive)
		.addProperty("tag", &GameObject::GetTag, &GameObject::SetTag)
		.endClass()
		.deriveClass<Quad, GameObject>("Quad")
		.addConstructor<void(*) (Vector<glm::vec2>, float, Vector<glm::vec2>, short, Material*)>()
		.addFunction("GetMaterial", &Quad::GetMaterial)
		.addProperty("sortingLayer", &Quad::GetSortingLayer, &Quad::SetSortingLayer)
		.addProperty("visible", &Quad::GetVisibility, &Quad::SetVisibility)
		.endClass()
		.deriveClass<Image, Quad>("Image")
		.addConstructor<void(*) (Vector<glm::vec2>, float, Vector<glm::vec2>, short, bool, string)>()
		.addFunction("FlipX", &Image::FlipX)
		.addFunction("FlipY", &Image::FlipY)
		.endClass()
		.deriveClass<Sprite, Image>("Sprite")
		.addConstructor<void(*) (Vector<glm::vec2>, float, Vector<glm::vec2>, short, bool, string)>()
		.addFunction("SetTexelSize", &Sprite::SetTexelSize)
		.addFunction("SetFrame", &Sprite::SetFrame)
		.endClass()
		.deriveClass<MovieQuad, GameObject>("MovieQuad")
		.addConstructor<void(*) (Vector<glm::vec2>, float, Vector<glm::vec2>, short, string, float)>()
		.addFunction("Play", &MovieQuad::Play)
		.addFunction("Stop", &MovieQuad::Stop)
		.endClass()
		.beginClass<PhysicsWorld>("PhysicsWorld")
		.addStaticFunction("GetInstance", &PhysicsWorld::GetInstance)
		.addFunction("SetSimulationUnitConversion", &PhysicsWorld::SetSimulationUnitConversion)
		.addFunction("SetGravity", &PhysicsWorld::SetGravity)
		.addProperty("debugDraw", &PhysicsWorld::GetDebugDraw, &PhysicsWorld::SetDebugDraw)
		.endClass()
		.beginClass<Material>("Material")
		.addConstructor<void(*) (string, bool)>()
		.addFunction("SetInt", &Material::SetInt)
		.addFunction("SetFloat", &Material::SetFloat)
		.addFunction("SetVec2", &Material::SetVec2)
		.addFunction("SetVec3", &Material::SetVec3)
		.addFunction("SetVec4", &Material::SetVec4)
		.addFunction("SetTexture", &Material::SetTexture)
		.endClass()
		.beginClass<Transform>("Transform")
		.addProperty("position", &Transform::LocalPosition, &Transform::SetPosition)
		.addProperty("rotation", &Transform::LocalRotation, &Transform::SetRotation)
		.addProperty("scale", &Transform::LocalScale, &Transform::SetScale)
		.addFunction("Translate", &Transform::Translate)
		.addFunction("Rotate", &Transform::Rotate)
		.addFunction("ScaleObject", &Transform::ScaleObject)
		.addFunction("SetParent", &Transform::SetParent)
		.addFunction("GetChildAt", &Transform::GetChildAt)
		.addFunction("RemoveChildAt", &Transform::RemoveChildAt)
		.endClass()
		.beginClass<RigidBody>("RigidBody")
		.addProperty("active", &RigidBody::GetActiveState, &RigidBody::SetActive)
		.addProperty("bodyType", &RigidBody::GetBodyType, &RigidBody::SetBodyType)
		.addProperty("gravityScale", &RigidBody::GetGavityScale, &RigidBody::SetGavityScale)
		.addProperty("fixedRotation", &RigidBody::GetFixedRotation, &RigidBody::SetFixedRotation)
		.addProperty("linearVelocity", &RigidBody::GetLinearVelocity, &RigidBody::SetLinearVelocity)
		.addProperty("angularVelocity", &RigidBody::GetAngularVelocity, &RigidBody::SetAngularVelocity)
		.addProperty("linearDampening", &RigidBody::GetLinearDamping, &RigidBody::SetLinearDamping)
		.addProperty("angularDampening", &RigidBody::GetAngularDamping, &RigidBody::SetAngularDamping)
		.addProperty("bullet", &RigidBody::GetBullet, &RigidBody::SetBullet)
		.addFunction("SetContactEnterCallback", &RigidBody::SetOnContactEnterCallback)
		.addFunction("SetContactExitCallback", &RigidBody::SetOnContactExitCallback)
		.addFunction("SetPreSolveCallback", &RigidBody::SetPreSolveCallback)
		.addFunction("ApplyLinearImpulse", &RigidBody::ApplyLinearImpulse)
		.addFunction("ApplyAngularImpulse", &RigidBody::ApplyAngularImpulse)
		.addFunction("ApplyForce", &RigidBody::ApplyForce)
		.addFunction("ApplyTorque", &RigidBody::ApplyTorque)
		.addFunction("AddBoxCollider", &RigidBody::AddBoxCollider)
		.addFunction("AddCircleCollider", &RigidBody::AddCircleCollider)
		.addFunction("AddPolygonCollider", &RigidBody::AddPolygonCollider)
		.endClass()
		.beginClass<CollisionInfo>("CollisionInfo")
		.addData("normal", &CollisionInfo::normal, false)
		.addData("impactVelocity", &CollisionInfo::impactVelocity, false)
		.addData("gameObject", &CollisionInfo::gameObject, false)
		.addData("pointCount", &CollisionInfo::pointCount, false)
		.addFunction("GetPoint", &CollisionInfo::GetPoint)
		.endClass()
		.beginClass<b2Filter>("FilterData")
		.addConstructor<void(*) ()>()
		.addData("category", &b2Filter::categoryBits)
		.addData("group", &b2Filter::groupIndex)
		.addData("mask", &b2Filter::maskBits)
		.endClass()
		.beginClass<FixtureWrapper>("Collider")
		.addProperty("sensor", &FixtureWrapper::IsSensor, &FixtureWrapper::SetSensor)
		.addProperty("friction", &FixtureWrapper::GetFriction, &FixtureWrapper::SetFriction)
		.addProperty("restitution", &FixtureWrapper::GetRestitution, &FixtureWrapper::SetRestitution)
		.addProperty("filterData", &FixtureWrapper::GetFilterData, &FixtureWrapper::SetFilterData)
		.addFunction("TestPoint", &FixtureWrapper::TestPoint)
		.addFunction("SetFilterGroup", &FixtureWrapper::SetFilterGroup)
		.addFunction("SetFilterMask", &FixtureWrapper::SetFilterMask)
		.addFunction("SetFilterCategory", &FixtureWrapper::SetFilterCategory)
		.addFunction("FlipShape", &FixtureWrapper::FlipShape)
		.endClass()
		.beginClass<b2Contact>("b2Contact")
		.addFunction("SetEnabled", &b2Contact::SetEnabled)
		.endClass()
		.beginClass<Input>("Input")
		.addStaticFunction("GetInstance", &Input::GetInstance)
		.addFunction("GetKeyDown", &Input::GetKeyDown)
		.addFunction("GetKeyUp", &Input::GetKeyUp)
		.addFunction("GetKey", &Input::GetKey)
		.addFunction("GetMouseButton", &Input::GetMouseButton)
		.addFunction("GetMouseButtonDown", &Input::GetMouseButtonDown)
		.addFunction("GetMouseButtonUp", &Input::GetMouseButtonUp)
		.addFunction("GetScrollWheel", &Input::GetScrollWheel)
		.addFunction("GetJoystickButton", &Input::GetJoystickButton)
		.addFunction("GetJoystickButtonUp", &Input::GetJoystickButtonUp)
		.addFunction("GetJoystickButtonDown", &Input::GetJoystickButtonDown)
		.addFunction("GetFirstAvailableController", &Input::GetFirstAvailableController)
		.addFunction("GetJoystickAxis", &Input::GetJoystickAxis)
		.addFunction("GetScreenCursorPosition", &Input::GetScreenCursorPosition)
		.addFunction("GetWorldCursorPosition", &Input::GetWorldCursorPosition)
		.endClass()
		.beginClass<ResourceManager>("Resources")
		.addStaticFunction("GetInstance", &ResourceManager::GetInstance)
		.addFunction("LoadSound", &ResourceManager::LoadSound)
		.addFunction("LoadTexture", &ResourceManager::LoadTexture)
		.addFunction("LoadFont", &ResourceManager::LoadFont)
		.addFunction("LoadAnimation", &ResourceManager::LoadAnimation)
		.addFunction("RegisterLoadingCallback", &ResourceManager::RegisterLoadingCallback)
		.addFunction("EnebleLoadingQueue", &ResourceManager::EnebleLoadingQueue)
		.addFunction("ProcessLoadingQueue", &ResourceManager::ProcessLoadingQueue)
		.endClass()
		.beginClass<SoundManager::SoundSource>("SoundSource")
		.addConstructor<void(*) (string)>()
		.addFunction("Play", &SoundManager::SoundSource::Play)
		.addFunction("Stop", &SoundManager::SoundSource::Stop)
		.addFunction("Pause", &SoundManager::SoundSource::Pause)
		.addFunction("Reset", &SoundManager::SoundSource::ReSet)
		.addFunction("SetPosition", &SoundManager::SoundSource::SetPosition)
		.addFunction("SetVelocity", &SoundManager::SoundSource::SetVelocity)
		.addFunction("SetVolume", &SoundManager::SoundSource::SetVolume)
		.addFunction("SetRollOffFactor", &SoundManager::SoundSource::SetRollOffFactor)
		.addFunction("SetReferenceDistance", &SoundManager::SoundSource::SetReferenceDistance)
		.addFunction("SetPitch", &SoundManager::SoundSource::SetPitch)
		.addFunction("SetMaxDistance", &SoundManager::SoundSource::SetMaxDistance)
		.endClass()
		.beginClass<SoundListener>("SoundListener")
		.addStaticFunction("SetPosition", SoundListener::SetPosition)
		.addStaticFunction("SetVelocity", SoundListener::SetVelocity)
		.addStaticFunction("SetGain", SoundListener::SetGain)
		.endClass()
		.beginClass<Camera>("Camera")
		.addConstructor<void(*) (Vector<glm::vec2>, bool)>()
		.addStaticFunction("GetMainCamera", &Camera::GetMainCamera)
		.addFunction("GetTransform", &Camera::GetTransform)
		.addFunction("SetViewport", &Camera::SetViewport)
		.addFunction("ScreenSplit", &Camera::ScreenSplit)
		.addProperty("enabled", &Camera::IsActive, &Camera::SetActive)
		.addProperty("size", &Camera::Size, &Camera::SetSize)
		.addProperty("width", &Camera::GetWidth)
		.addProperty("heigth", &Camera::GetHeight)
		.endClass()
		.beginClass<SpriteAnimator>("SpriteAnimator")
		.addStaticFunction("GetInstance", &SpriteAnimator::GetInstance)
		.addFunction("RegisterAnimation", &SpriteAnimator::RegisterAnimation)
		.addFunction("RegisterFrameEvent", &SpriteAnimator::RegisterFrameEvent)
		.addFunction("RegisterQuad", &SpriteAnimator::RegisterQuad)
		.addFunction("SetAnimationSpeed", &SpriteAnimator::SetAnimationSpeed)
		.addFunction("SetAnimationScale", &SpriteAnimator::SetAnimationScale)
		.addFunction("PlayAnimation", &SpriteAnimator::PlayAnimation)
		.addFunction("PauseAnimation", &SpriteAnimator::PauseAnimation)
		.addFunction("ResumeAnimation", &SpriteAnimator::ResumeAnimation)
		.addFunction("StopAnimation", &SpriteAnimator::StopAnimation)
		.endClass()
		.beginClass<LightScreen>("LightScreen")
		.addStaticFunction("GetInstance", &LightScreen::GetInstance)
		.addFunction("SetAmbientColor", &LightScreen::SetAmbientColor)
		.endClass()
		.beginClass<LightScreen::LightSource>("LightSource")
		.addConstructor<void(*) (Vector<glm::vec2>, Vector<glm::vec3>, float, float, bool)>()
		.addProperty("position", &LightScreen::LightSource::GetPosition, &LightScreen::LightSource::SetPosition)
		.addProperty("color", &LightScreen::LightSource::GetColor, &LightScreen::LightSource::SetColor)
		.addProperty("radious", &LightScreen::LightSource::GetRadious, &LightScreen::LightSource::SetRadious)
		.addProperty("brightness", &LightScreen::LightSource::GetBrightness, &LightScreen::LightSource::SetBrightness)
		.addProperty("enabled", &LightScreen::LightSource::GetActive, &LightScreen::LightSource::SetActive)
		.endClass()
		.beginClass<Gui>("GUI")
		.addStaticFunction("GetInstance", &Gui::GetInstance)
		.addFunction("Label", &Gui::Label)
		.addFunction("Button", &Gui::Button)
		.addFunction("Image", &Gui::Image)
		.addFunction("SetFontColor", &Gui::SetFontColor)
		.endClass()
		.beginClass<FrameBuffer>("PostProcessing")
		.addStaticFunction("GetInstance", &FrameBuffer::GetInstance)
		.addFunction("SetShader", &FrameBuffer::SetShader)
		.addProperty("enabled", &FrameBuffer::IsEnabled, &FrameBuffer::SetEnabled)
		.addProperty("includeGUI", &FrameBuffer::IsGuiIncluded, &FrameBuffer::SetIncludeGUI)
		.addFunction("SetInt", &FrameBuffer::SetInt)
		.addFunction("SetFloat", &FrameBuffer::SetFloat)
		.addFunction("SetVec2", &FrameBuffer::SetVec2)
		.addFunction("SetVec3", &FrameBuffer::SetVec3)
		.addFunction("SetVec4", &FrameBuffer::SetVec4)
		.addFunction("SetTexture", &FrameBuffer::SetTexture)
		.endClass()
		.beginClass<FiniteStateMachine>("FSM")
		.addConstructor<void(*) ()>()
		.addProperty("currentState", &FiniteStateMachine::GetCurrentState)
		.addFunction("AddState", &FiniteStateMachine::AddState)
		.addFunction("SetStartState", &FiniteStateMachine::SetStartState)
		.addFunction("SetParameter", &FiniteStateMachine::SetParameter)
		.addFunction("AddTransition", &FiniteStateMachine::AddTransition)
		.addFunction("RegisterCallback", &FiniteStateMachine::RegisterCallback)
		.endClass()
		.beginClass<Vector<glm::vec2>>("Vec2")
		.addConstructor<void(*) (float, float)>()
		.addProperty("x", &Vector<glm::vec2>::Get<0>, &Vector<glm::vec2>::Set<0>)
		.addProperty("y", &Vector<glm::vec2>::Get<1>, &Vector<glm::vec2>::Set<1>)
		.addFunction("__add", &Vector<glm::vec2>::operator+)
		.addFunction("__eq", &Vector<glm::vec2>::operator==)
		.addFunction("__sub", &Vector<glm::vec2>::operator-)
		.addFunction("__mul", &Vector<glm::vec2>::operator*)
		.addFunction("__div", &Vector<glm::vec2>::operator/)
		.addFunction("ScalarMult", &Vector<glm::vec2>::ScalarMultiply)
		.addFunction("Negate", &Vector<glm::vec2>::Negate)
		.addFunction("Dot", &Vector<glm::vec2>::Dot)
		.addFunction("Reflect", &Vector<glm::vec2>::Reflect)
		.addFunction("Normalize", &Vector<glm::vec2>::Normalize)
		.endClass()
		.beginClass<Vector<glm::vec3>>("Vec3")
		.addConstructor<void(*) (float, float, float)>()
		.addProperty("x", &Vector<glm::vec3>::Get<0>, &Vector<glm::vec3>::Set<0>)
		.addProperty("y", &Vector<glm::vec3>::Get<1>, &Vector<glm::vec3>::Set<1>)
		.addProperty("z", &Vector<glm::vec3>::Get<2>, &Vector<glm::vec3>::Set<2>)
		.addFunction("__add", &Vector<glm::vec3>::operator+)
		.addFunction("__eq", &Vector<glm::vec3>::operator==)
		.addFunction("__sub", &Vector<glm::vec3>::operator-)
		.addFunction("__mul", &Vector<glm::vec3>::operator*)
		.addFunction("__div", &Vector<glm::vec3>::operator/)
		.addFunction("ScalarMult", &Vector<glm::vec3>::ScalarMultiply)
		.addFunction("Negate", &Vector<glm::vec3>::Negate)
		.addFunction("Dot", &Vector<glm::vec3>::Dot)
		.addFunction("Reflect", &Vector<glm::vec3>::Reflect)
		.addFunction("Normalize", &Vector<glm::vec3>::Normalize)
		.endClass()
		.beginClass<Vector<glm::vec4>>("Vec4")
		.addConstructor<void(*) (float, float, float, float)>()
		.addProperty("x", &Vector<glm::vec4>::Get<0>, &Vector<glm::vec4>::Set<0>)
		.addProperty("y", &Vector<glm::vec4>::Get<1>, &Vector<glm::vec4>::Set<1>)
		.addProperty("z", &Vector<glm::vec4>::Get<2>, &Vector<glm::vec4>::Set<2>)
		.addProperty("w", &Vector<glm::vec4>::Get<3>, &Vector<glm::vec4>::Set<3>)
		.addFunction("__add", &Vector<glm::vec4>::operator+)
		.addFunction("__eq", &Vector<glm::vec4>::operator==)
		.addFunction("__sub", &Vector<glm::vec4>::operator-)
		.addFunction("__mul", &Vector<glm::vec4>::operator*)
		.addFunction("__div", &Vector<glm::vec4>::operator/)
		.addFunction("ScalarMult", &Vector<glm::vec4>::ScalarMultiply)
		.addFunction("Negate", &Vector<glm::vec4>::Negate)
		.addFunction("Dot", &Vector<glm::vec4>::Dot)
		.addFunction("Reflect", &Vector<glm::vec4>::Reflect)
		.addFunction("Normalize", &Vector<glm::vec4>::Normalize)
		.endClass()
		.beginClass<JSonParser>("JsonObject")
		.addConstructor<void(*) (const char*)>()
		.addFunction("GetInt", &JSonParser::GetInt)
		.addFunction("GetFloat", &JSonParser::GetFloat)
		.addFunction("GetBool", &JSonParser::GetBool)
		.addFunction("GetVec2", &JSonParser::GetVec2)
		.addFunction("GetVec3", &JSonParser::GetVec3)
		.addFunction("GetVec4", &JSonParser::GetVec4)
		.addFunction("GetString", &JSonParser::GetString)
		.endClass()
		.beginClass<TiledMapParser>("TiledMapParser")
		.addStaticFunction("GetInstance", &TiledMapParser::GetInstance)
		.addFunction("ParseFile", &TiledMapParser::ParseFile)
		.endClass()
		.beginClass<TiledMap>("TiledMap")
		.addData("width", &TiledMap::width, false)
		.addData("height", &TiledMap::height, false)
		.addData("tileWidth", &TiledMap::tileWidth, false)
		.addData("tileHeight", &TiledMap::tileHeight, false)
		.addData("orientation", &TiledMap::orientation, false)
		.addData("version", &TiledMap::version, false)
		.addData("renderOrder", &TiledMap::renderOrder, false)
		.addData("nextObjectID", &TiledMap::nextObjectID, false)
		.addFunction("GetProperty", &TiledMap::GetProperty)
		.addFunction("GetTileset", &TiledMap::GetTileset)
		.addFunction("GetLayer", &TiledMap::GetLayer)
		.endClass()
		.beginClass<TiledTileset>("TiledTileSet")
		.addData("columns", &TiledTileset::columns, false)
		.addData("firstGid", &TiledTileset::firstGid, false)
		.addData("image", &TiledTileset::image, false)
		.addData("name", &TiledTileset::name, false)
		.addData("tileWidth", &TiledTileset::tileWidth, false)
		.addData("tileHeight", &TiledTileset::tileHeight, false)
		.addData("imageWidth", &TiledTileset::imageWidth, false)
		.addData("imageHeight", &TiledTileset::imageHeight, false)
		.addData("margin", &TiledTileset::margin, false)
		.addData("spacing", &TiledTileset::spacing, false)
		.addFunction("GetProperty", &TiledTileset::GetProperty)
		.addFunction("GetTileProperty", &TiledTileset::GetTileProperty)
		.endClass()
		.beginClass<TiledLayer>("TiledLayer")
		.addData("width", &TiledLayer::width, false)
		.addData("height", &TiledLayer::height, false)
		.addData("name", &TiledLayer::name, false)
		.addData("type", &TiledLayer::type, false)
		.addData("visible", &TiledLayer::visible, false)
		.addData("x", &TiledLayer::x, false)
		.addData("y", &TiledLayer::y, false)
		.addData("opacity", &TiledLayer::opacity, false)
		.addData("objectCount", &TiledLayer::objectCount, false)
		.addFunction("GetProperty", &TiledLayer::GetProperty)
		.addFunction("GetTiledObject", &TiledLayer::GetTiledObject)
		.addFunction("GetTiledObjectByName", &TiledLayer::GetTiledObjectByName)
		.addFunction("GetTile", &TiledLayer::GetTile)
		.endClass()
		.beginClass<TiledObject>("TiledObject")
		.addData("id", &TiledObject::id, false)
		.addData("width", &TiledObject::width, false)
		.addData("height", &TiledObject::height, false)
		.addData("name", &TiledObject::name, false)
		.addData("type", &TiledObject::type, false)
		.addData("visible", &TiledObject::visible, false)
		.addData("x", &TiledObject::x, false)
		.addData("y", &TiledObject::y, false)
		.addData("rotation", &TiledObject::rotation, false)
		.addData("gid", &TiledObject::gid, false)
		.addData("ellipse", &TiledObject::ellipse, false)
		.addData("polyline", &TiledObject::polyline, false)
		.addData("polygon", &TiledObject::polygon, false)
		.addData("pointCount", &TiledObject::pointCount, false)
		.addFunction("GetProperty", &TiledObject::GetProperty)
		.addFunction("GetPoint", &TiledObject::GetPoint)
		.endClass();
}