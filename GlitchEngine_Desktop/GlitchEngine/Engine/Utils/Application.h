#pragma once
#include <map>
#include <glm/glm.hpp>
#include <Utils/Vector.h>

using namespace std;

class ApplicationData
{

public:
	ApplicationData();
	~ApplicationData();

	static ApplicationData &GetInstance();
	void Destroy();

	float currentAspectRatio;
	float targetAspectRatio;
	bool autoScale;
	int renderHeight;
	int renderWidth;
	int maxLightsOnScreen;
	bool useLights;
	Vector<glm::vec3> ambientColor;
	Vector<glm::vec2> aspectScale;
	glm::vec4 baseSize;
	int designHeight;
	int designWidth;
};

