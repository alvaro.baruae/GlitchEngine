#pragma once

#include <Utils/ResourceManager.h>
#include <Utils/TiledUtils.h>
#include <Utils/Vector.h>

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <json/json.h>

using namespace std;

struct TiledObject
{
	int id;
	int width;
	int height;
	string name;
	string type;
	bool visible;
	int x;
	int y;
	float rotation;
	int gid;
	bool ellipse;
	bool polyline;
	bool polygon;
	int pointCount;

	vector<Vector<glm::vec2>> points;
	Json::Value properties;

	string GetProperty(string id)
	{
		return properties[id].asString();
	}

	Vector<glm::vec2> GetPoint(int index)
	{
		return points[index];
	}

};

struct TiledLayer
{
	int width;
	int height;
	string name;
	string type;
	bool visible;
	int x;
	int y;
	float opacity;
	vector<int> data;
	int objectCount;
	vector<TiledObject > objects;

	Json::Value properties;

	string GetProperty(string id)
	{
		return properties[id].asString();
	}

	TiledObject GetTiledObjectByName(string name)
	{
		for (int i = 0; i < objects.size(); i++)
			if(objects[i].name == name)
				return objects[i];
	}

	TiledObject GetTiledObject(int index)
	{
		return objects[index];
	}

	int GetTile(int index)
	{
		return data[index];
	}
};

struct TiledTileset
{
	int firstGid;
	string image;
	string name;
	int tileWidth;
	int tileHeight;
	int imageWidth;
	int imageHeight;
	int margin;
	int spacing;
	int columns;

	Json::Value properties;
	Json::Value tileProperties;

	string GetTileProperty(int tile, string id)
	{
		return tileProperties[to_string(tile)][id].asString();
	}

	string GetProperty(string id)
	{
		return properties[id].asString();
	}
};

struct TiledMap
{
	int width;
	int height;
	int tileWidth;
	int tileHeight;
	string orientation;
	string version;
	string renderOrder;
	int nextObjectID;

	map<string, TiledLayer> layers;
	map<string, TiledTileset> tileSets;
	Json::Value properties;

	string GetProperty(string id)
	{
		return properties[id].asString();
	}

	TiledTileset GetTileset(string tilesetName)
	{
		return tileSets[tilesetName];
	}

	TiledLayer GetLayer(string layerName)
	{
		return layers[layerName];
	}
};

class TiledMapParser
{
private:
	ResourceManager* RM;

public:
	TiledMapParser();
	~TiledMapParser();

	static TiledMapParser &GetInstance();
	void Destroy();

	TiledMap ParseFile(string filePath);
};