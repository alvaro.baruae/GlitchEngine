#include "WindowHandler.h"



WindowHandler::WindowHandler()
{
}


WindowHandler::~WindowHandler()
{
}

WindowHandler &WindowHandler::GetInstance()
{
	static WindowHandler* ins;
	if (ins == NULL)
		ins = new WindowHandler();
	return *ins;
}

void WindowHandler::Destroy()
{
	delete &GetInstance();
}