#pragma once

#include <Box2D/Box2D.h>
#include <Utils/Vector.h>


class FixtureWrapper
{
private:
	b2Fixture* fixture;

public:
	FixtureWrapper(b2Fixture* fixt);
	b2Fixture* GetFixture();

	~FixtureWrapper();

	bool IsSensor() const;
	void SetSensor(bool sensor);

	float GetFriction() const;
	void SetFriction(float friction);

	float GetRestitution() const;
	void SetRestitution(float restitution);

	bool TestPoint(Vector<glm::vec2> point);

	b2Filter GetFilterData() const;
	void SetFilterData(const b2Filter& data);

	void SetFilterGroup(uint16 group);
	void SetFilterMask(uint16 mask);
	void SetFilterCategory(uint16 category);

	void FlipShape();
};