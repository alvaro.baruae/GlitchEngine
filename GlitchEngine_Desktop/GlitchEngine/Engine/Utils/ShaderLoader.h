#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <Utils/Logger.h>
#include <Utils/Application.h>
#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <cstdio>
#include <string>
#include <regex>
#include <glew.h>

using namespace std;

class GameObject;
class Renderer;
class ShaderLoader
{
    private:
        string ReadFromFile(const char* source);
        GLuint CompileShader(GLenum mode, const char* source);
		map<string, string> ExtractAttributes(string& shaderVert);
		vector<GLfloat> quadVerts;

		void PrintShaderInfo(GLuint shader);
		void PrintInfoLog(GLuint programme);
		const char* GLTypeToString(GLenum type);
		static map<string, map<string, string>> ShaderAttributes;
		static map<GLuint, GLint> cameraUniformLocations;

    public:
        ShaderLoader();
        ~ShaderLoader(void);
        void LoadShader(string shaderName, string fullFileLocation);
		void LoadShader(string shaderName, string vert, string frag);

		static GLuint VertexBufferID;
		static map<string, GLuint> ProgramIDs;
		static GLuint GetProgramIDAt(string key)
		{
            return ProgramIDs[key];
        }

		static GLuint GetCameraUniformLocation(GLuint key)
		{
			return cameraUniformLocations[key];
		}
		
		static map<string, string> GetAttributes(string shader)
		{
			return ShaderAttributes[shader];
		}
};