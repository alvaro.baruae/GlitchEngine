#include <Utils/JSonParser.h>

JSonParser::JSonParser(const char* fileLocation)
{
	std::ifstream config_doc("Resources/" + std::string(fileLocation), std::ifstream::binary);
	config_doc >> root;
}

JSonParser::~JSonParser() {}


bool JSonParser::GetBool(const char* path)
{
	return GetField(path).asBool();
}

int JSonParser::GetInt(const char* path)
{
	return GetField(path).asInt();
}

int JSonParser::GetFloat(const char * path)
{
	return GetField(path).asFloat();
}

std::string JSonParser::GetString(const char* path)
{
	return GetField(path).asString();
}

Json::Value JSonParser::GetField(const char *path)
{
	std::vector<std::string> parts;

	std::stringstream ss(path);
	std::string item;
	while (getline(ss, item, '.'))
		parts.push_back(item);

	Json::Value element = Json::Value::null;

	for (unsigned int i = 0; i < parts.size(); i++)
	{
		if (element == Json::Value::null)
			element = root[parts[i].c_str()];
		else
			element = element[parts[i].c_str()];
	}

	return element;
}

Vector<glm::vec2> JSonParser::GetVec2(const char* path)
{
	std::vector<std::string> parts;
	std::stringstream ss(GetField(path).asCString());
	std::string item;

	while (getline(ss, item, ','))
		parts.push_back(item);

	return Vector<glm::vec2>(stof(parts[0]), stof(parts[1]));
}

Vector<glm::vec3> JSonParser::GetVec3(const char* path)
{
	std::vector<std::string> parts;
	std::stringstream ss(GetField(path).asCString());
	std::string item;

	while (getline(ss, item, ','))
		parts.push_back(item);

	return Vector<glm::vec3>(stof(parts[0]), stof(parts[1]), stof(parts[2]));
}

Vector<glm::vec4> JSonParser::GetVec4(const char* path)
{
	std::vector<std::string> parts;
	std::stringstream ss(GetField(path).asCString());
	std::string item;

	while (getline(ss, item, ','))
		parts.push_back(item);

	return Vector<glm::vec4>(stof(parts[0]), stof(parts[1]), stof(parts[2]), stof(parts[3]));
}