#include "PhysicsDebugDraw.h"

PhysicsDebugDraw::PhysicsDebugDraw()
{
}

PhysicsDebugDraw::~PhysicsDebugDraw()
{
}

void PhysicsDebugDraw::InitDraw()
{
	unitConversion = (&PhysicsWorld::GetInstance())->GetSimulationUnits();
	mainCamera = Camera::GetMainCamera();

	glPushMatrix();
	glOrtho(-mainCamera->GetWidth() / 2.f / mainCamera->Size(), mainCamera->GetWidth() / 2.f / mainCamera->Size(), -mainCamera->GetHeight() / 2.f / mainCamera->Size(), mainCamera->GetHeight() / 2.f / mainCamera->Size(), -1.f, 1000.f);
	glTranslatef(-mainCamera->GetTransform()->WorldPosition().Get<0>(), -mainCamera->GetTransform()->WorldPosition().Get<1>(), 0);
}

void PhysicsDebugDraw::EndDraw()
{
	glPopMatrix();
}

void PhysicsDebugDraw::DrawPolygon(const b2Vec2 * vertices, int32 vertexCount, const b2Color & color)
{

}

void PhysicsDebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	glColor4f(color.r, color.g, color.b, 0.25f);
	glBegin(GL_POLYGON);
	for (int i = 0; i < vertexCount; i++)
		glVertex2f(vertices[i].x * unitConversion, vertices[i].y * unitConversion);
	glEnd();
}

void PhysicsDebugDraw::DrawCircle(const b2Vec2 & center, float32 radius, const b2Color & color)
{
}

void PhysicsDebugDraw::DrawSolidCircle(const b2Vec2 & center, float32 radius, const b2Vec2 & axis, const b2Color & color)
{
}

void PhysicsDebugDraw::DrawSegment(const b2Vec2 & p1, const b2Vec2 & p2, const b2Color & color)
{
}

void PhysicsDebugDraw::DrawTransform(const b2Transform & xf)
{
}
