#include "FiniteStateMachine.h"

bool FiniteStateMachine::CheckTransition(FSMTransition trans)
{
	switch (trans.operation)
	{
	case FSMOperations::EQ:
		if (*trans.value1 == *trans.value2)
			return true;
		break;
	case FSMOperations::NEQ:
		if (*trans.value1 != *trans.value2)
			return true;
		break;
	case FSMOperations::LEQ:
		if (*trans.value1 <= *trans.value2)
			return true;
		break;
	case FSMOperations::GEQ:
		if (*trans.value1 >= *trans.value2)
			return true;
		break;
	case FSMOperations::G:
		if (*trans.value1 > *trans.value2)
			return true;
		break;
	case FSMOperations::L:
		if (*trans.value1 < *trans.value2)
			return true;
		break;
	}

	return false;
}

void FiniteStateMachine::UpdateState()
{
	for (const auto it : states[currentState].transitions)
	{
		if (CheckTransition(it))
		{
			currentState = it.toState;
			LED->QueueLuaEvent(callbackIndex);
			UpdateState();
			break;
		}
	}
}

FiniteStateMachine::FiniteStateMachine()
{
	LED = &LuaEventDispatcher::GetInstance();
}


FiniteStateMachine::~FiniteStateMachine()
{
}

string FiniteStateMachine::GetCurrentState() const
{
	return currentState;
}

void FiniteStateMachine::AddState(string name)
{
	FSMState state;
	state.key = name;
	states[name] = state;
}

void FiniteStateMachine::SetStartState(string name)
{
	map<string, FSMState>::iterator it = states.find(name);
	if (it != states.end())
		currentState = name;
	else
	{
		Logger::Log("Error, not a valid start state");
		return;
	}
}

void FiniteStateMachine::SetParameter(string name, float value)
{
	parameters[name] = value;
	UpdateState();
}

void FiniteStateMachine::AddTransition(string stateName, string destinationStateName, unsigned int operation, string param1, string param2)
{
	FSMState currentState;
	FSMState destinationState;

	map<string, FSMState>::iterator it = states.find(stateName);
	if (it != states.end())
		currentState = states[stateName];
	else
	{
		Logger::Log("Error, not a valid start state");
		return;
	}
	
	it = states.find(destinationStateName);
	if (it != states.end())
		destinationState = states[destinationStateName];
	else
	{
		Logger::Log("Error, not a valid end state");
		return;
	}
	
	FSMTransition transition;
	transition.operation = (FSMOperations)operation;

	map<string, float>::iterator pIt, pIt2;
	pIt = parameters.find(param1);
	pIt2 = parameters.find(param2);

	if(pIt != parameters.end())
		transition.value1 = &parameters[param1];
	else
	{
		Logger::Log("Error, not a valid name for parameter 1");
		return;
	}

	if (pIt2 != parameters.end())
		transition.value2 = &parameters[param2];
	else
	{
		Logger::Log("Error, not a valid name for parameter 2");
		return;
	}

	transition.toState = destinationStateName;
	currentState.transitions.push_back(transition);
	states[stateName] = currentState;
}

void FiniteStateMachine::RegisterCallback()
{
	callbackIndex = LED->GetLuaFunctionReference();
}
