#include "TiledMapParser.h"

TiledMapParser::TiledMapParser()
{
	RM = &ResourceManager::GetInstance();
}


TiledMapParser::~TiledMapParser()
{
}

TiledMapParser &TiledMapParser::GetInstance(){

	static TiledMapParser *instance = NULL;

	if (instance == NULL)
		instance = new TiledMapParser();

	return *instance;
}

void TiledMapParser::Destroy()
{

	TiledMapParser *ins = &GetInstance();
	if (ins != NULL)
		delete ins;
}

TiledMap TiledMapParser::ParseFile(string filePath)
{
	TiledMap map;
	Json::Value root;
	std::ifstream config_doc("Resources/" + filePath, std::ifstream::binary);
	config_doc >> root;

	map.width = root["width"].asInt();
	map.height = root["height"].asInt();
	map.tileWidth = root["tilewidth"].asInt();
	map.tileHeight = root["tileheight"].asInt();
	map.orientation = root["orientation"].asString();
	map.version = root["version"].asString();
	map.renderOrder = root["renderorder"].asString();
	map.properties = root["properties"];
	map.nextObjectID = root["nextobjectid"].asInt();

	for (Json::Value it : root["tilesets"])
	{
		TiledTileset t;
		t.columns = it["columns"].asInt();
		t.firstGid = it["firstgid"].asInt();
		t.image = it["image"].asString();
		t.name = it["name"].asString();
		t.tileWidth = it["tilewidth"].asInt();
		t.tileHeight = it["tileHeight"].asInt();
		t.imageWidth = it["imagewidth"].asInt();
		t.imageHeight = it["imageheight"].asInt();
		t.margin = it["margin"].asInt();
		t.spacing = it["spacing"].asInt();
		t.properties = it["properties"];
		t.tileProperties = it["tileproperties"];
		map.tileSets[t.name] = t;
	}

	for (Json::Value it : root["layers"])
	{
		TiledLayer l;
		l.name = it["name"].asString();
		l.width = it["width"].asInt();
		l.height = it["height"].asInt();
		l.opacity = it["opacity"].asFloat();
		l.properties = it["properties"];
		l.type = it["type"].asString();
		l.visible = it["visible"].asBool();
		l.x = it["x"].asInt();
		l.y = it["y"].asInt();

		if (l.type == "tilelayer")
		{
			string data = decompress_string(base64_decode(it["data"].asString()));
			for (unsigned int i = 0; i < data.length(); i = i + 4)
			{
				char buff[4];
				buff[0] = data[i];
				buff[1] = data[i + 1];
				buff[2] = data[i + 2];
				buff[3] = data[i + 3];

				l.data.push_back(RM->BinaryCharToInt(buff, 4));
			}
		}		
		else
		{
			for (Json::Value it2 : it["objects"])
			{
				TiledObject o;
				o.gid = it2["gid"].asInt();
				o.id = it2["id"].asInt();
				o.width = it2["width"].asInt();
				o.height = it2["height"].asInt();
				o.name = it2["name"].asString();
				o.properties = it2["properties"];
				o.x = it2["x"].asInt();
				o.y = it2["y"].asInt();
				o.rotation = it2["rotation"].asFloat();
				o.type = it2["type"].asString();
				o.visible = it2["visible"].asBool();
				o.ellipse = it2["ellipse"].asBool();
				o.polygon = false;
				o.polyline = false;

				for (Json::Value it3 : it2["polyline"])
				{
					o.polyline = true;
					Vector<glm::vec2> p(it3["x"].asInt(), it3["y"].asInt());
					o.points.push_back(p);
				}

				for (Json::Value it3 : it2["polygon"])
				{
					o.polygon = true;
					Vector<glm::vec2> p(it3["x"].asInt(), it3["y"].asInt());
					o.points.push_back(p);
				}

				o.pointCount = o.points.size();
				l.objects.push_back(o);
			}
			l.objectCount = l.objects.size();
		}
		map.layers[l.name] = l;
	}

	return map;
}