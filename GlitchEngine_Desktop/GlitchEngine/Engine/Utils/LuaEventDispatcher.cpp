#include <Utils/LuaEventDispatcher.h>
#include <Classes/RigidBody.h>

LuaEventDispatcher::LuaEventDispatcher()
{

}


LuaEventDispatcher::~LuaEventDispatcher()
{
}

LuaEventDispatcher &LuaEventDispatcher::GetInstance()
{
	static LuaEventDispatcher *led = NULL;

	if (led == NULL)
		led = new LuaEventDispatcher();

	return *led;
}

void LuaEventDispatcher::SetLuaState(lua_State *state)
{
	L = state;
	lua_newtable(L);
	tableIndex = luaL_ref(L, LUA_REGISTRYINDEX);
}

lua_State* LuaEventDispatcher::GetLuaState()
{
	return L;
}

int LuaEventDispatcher::GetLuaFunctionReference()
{
	int t = luaL_ref(L, LUA_REGISTRYINDEX);
	lua_rawgeti(L, LUA_REGISTRYINDEX, tableIndex);
	lua_rawgeti(L, LUA_REGISTRYINDEX, t);
	t = luaL_ref(L, -2);
	lua_pop(L, 2);
	return t;
}

void LuaEventDispatcher::QueueLuaCollisionEvent(int index, CollisionInfo data)
{
	collisionFunctionReferences[index] = data;
}

void LuaEventDispatcher::CallPreSolveEvent(int index, CollisionInfo data, b2Contact* contact)
{
	lua_rawgeti(L, LUA_REGISTRYINDEX, tableIndex);
	lua_rawgeti(L, -1, index);
	luabridge::push<CollisionInfo>(L, data);
	luabridge::push<b2Contact*>(L, contact);
	if (lua_pcall(L, 2, 0, 0))
	{
		lua_PrintError(L, "Collision Presolve Callback");
		return;
	}
	lua_pop(L, 1);
}

void LuaEventDispatcher::QueueLuaEvent(int index)
{
	offFunctionReferences.push_back(index);
}

void LuaEventDispatcher::TriggerLuaEvent()
{
	int t = luaL_ref(L, LUA_REGISTRYINDEX);
	lua_rawgeti(L, LUA_REGISTRYINDEX, tableIndex);
	lua_rawgeti(L, LUA_REGISTRYINDEX, t);
	t = luaL_ref(L, -2);
	lua_pop(L, 2);
	functionReferences.push_back(t);
}

void LuaEventDispatcher::Dispatch()
{
	for (int it : functionReferences)
	{
		lua_rawgeti(L, LUA_REGISTRYINDEX, tableIndex);
		lua_rawgeti(L, -1, it);
		if (lua_pcall(L, 0, 0, 0))
		{
			lua_PrintError(L, NULL);
			return;
		}
		lua_pop(L, 1);
	}

	for (auto const &it : collisionFunctionReferences)
	{
		lua_rawgeti(L, LUA_REGISTRYINDEX, tableIndex);
		lua_rawgeti(L, -1, it.first);
		luabridge::push<CollisionInfo>(L, it.second);
		if (lua_pcall(L, 1, 0, 0))
		{
			lua_PrintError(L, "Collision Callback");
			return;
		}
		lua_pop(L, 1);
	}

	functionReferences.clear();
	collisionFunctionReferences.clear();
	functionReferences = offFunctionReferences;
	offFunctionReferences.clear();
}

void LuaEventDispatcher::DispatchLoadingQueueEvents(int index, float progress)
{
	lua_rawgeti(L, LUA_REGISTRYINDEX, tableIndex);
	lua_rawgeti(L, -1, index);
	luabridge::push<float>(L, progress);
	if (lua_pcall(L, 1, 0, 0))
	{
		lua_PrintError(L, "Loading Queue");
		return;
	}
	lua_pop(L, 1);
}

void LuaEventDispatcher::Destroy()
{
	LuaEventDispatcher *led = &GetInstance();
	if (led != NULL)
		delete led;
}