#pragma once
#include <Box2D/Box2D.h>
#include <Classes/RigidBody.h>

class ContactListener :	public b2ContactListener
{
public:
	ContactListener();
	~ContactListener();

	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
	void PreSolve(b2Contact* contact, const b2Manifold* oldManifold);
	CollisionInfo GetCollisionInfo(b2Contact* contact);
};

