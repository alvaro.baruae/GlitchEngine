#pragma once

#include <map>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string>
#include <Utils/Application.h>
#include <Objects/Camera.h>

using namespace std;

enum EventType
{
	Keyboard,
	Mouse,
	Joystick,
};

class Input
{
public:
	Input();
	~Input();
	bool PollEvents();
	static Input &GetInstance();

	void Destroy();

	bool GetKeyDown(int key);
	bool GetKeyUp(int key);
	bool GetKey(int key);

	Vector<glm::vec2> GetScreenCursorPosition();
	Vector<glm::vec2> GetWorldCursorPosition();
	
	bool GetMouseButtonDown(int button);
	bool GetMouseButton(int button);
	bool GetMouseButtonUp(int button);
	int GetScrollWheel();

	bool GetJoystickButtonDown(int joystick, int button);
	bool GetJoystickButtonUp(int joystick, int button);
	bool GetJoystickButton(int joystick, int button);
	float GetJoystickAxis(int joystick, int axis);

	int GetFirstAvailableController();

private:

	SDL_Event e;
	const Uint8 *state;
	map<pair<EventType,int>, pair<int, int>> joyEventMap;

	int scrollWheel;

	map<int, bool> keyPressedMap;
	map<int, bool> mousePressedMap;

	map<int, map<int, bool> > joystickPressedMap;
	map<pair<int, int>, float> joystickAxisValue;

	map<int, bool> keyUpRegistered;
	map<int, bool> keyDownRegistered;

	map<int, bool> mouseUpRegistered;
	map<int, bool> mouseDownRegistered;

	map<int, map<int, bool> > joystickButtonUpRegistered;
	map<int, map<int, bool> > joystickButtonDownRegistered;
};