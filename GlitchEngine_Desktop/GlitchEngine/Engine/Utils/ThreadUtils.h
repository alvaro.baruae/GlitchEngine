#pragma once

#include <thread>
#include <queue>
#include <mutex>

template<typename T>
class SafeQueue
{
private:
	queue<T> q;
	mutex lockMutex;
public:
	void push(const T& data)
	{
		lock_guard<mutex> lock(lockMutex);
		q.push(data);
	}

	bool empty() const
	{
		lock_guard<mutex> lock(lockMutex);
		return q.empty();

	}

	T& front()
	{
		lock_guard<mutex> lock(lockMutex);
		return q.front();
	}

	T const& front() const
	{
		lock_guard<mutex> lock(lockMutex);
		return q.front();
	}

	void pop()
	{
		lock_guard<mutex> lock(lockMutex);
		q.pop();
	}

	size_t size()
	{
		lock_guard<mutex> lock(lockMutex);
		return q.size();
	}
};