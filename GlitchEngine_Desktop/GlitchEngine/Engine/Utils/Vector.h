#pragma once
#include <glm/glm.hpp>

template <class T>
class Vector
{
public:
	Vector(){}
	~Vector(){}

	template<typename = std::enable_if<std::is_same<T, glm::vec2>::value>>
	Vector(float x, float y)
	{
		vec = glm::vec2(x, y);
	}

	template<typename = std::enable_if<std::is_same<T, glm::vec3>::value>>
	Vector(float x, float y, float z)
	{
		vec = glm::vec3(x, y, z);
	}

	template<typename = std::enable_if<std::is_same<T, glm::vec4>::value>>
	Vector(float x, float y, float z, float w)
	{
		vec = glm::vec4(x, y, z, w);
	}

	float Dot(Vector<T> v)
	{
		return glm::dot(vec, v.vec);
	}

	Vector<T> ScalarMultiply(float val)
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec *= val;
		return ret;
	}

	Vector<T> Reflect(Vector<T> normal)
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec = glm::reflect(ret.vec, normal.vec);
		return ret;
	}

	Vector<T> Normalize()
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec = glm::normalize(ret.vec);
		return ret;
	}

	Vector<T> Negate()
	{
		Vector<T> ret;
		ret.vec = vec;
		return ret;
	}

	Vector<T> operator+(const Vector<T> &v)
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec += v.vec;
		return ret;
	}

	Vector<T> operator-(const Vector<T> &v)
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec -= v.vec;
		return ret;
	}

	Vector<T> operator*(const Vector<T> &v)
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec *= v.vec;
		return ret;
	}

	Vector<T> operator*=(const Vector<T> &v)
	{
		vec *= v.vec;
		return *this;
	}

	Vector<T> operator/(const Vector<T> &v)
	{
		Vector<T> ret;
		ret.vec = vec;
		ret.vec /= v.vec;
		return ret;
	}

	bool operator==(const Vector<T> &v)
	{
		return vec == v.vec;
	}

	bool operator!=(const Vector<T> &v)
	{
		return vec != v.vec;
	}

	template<unsigned index>
	float Get() const
	{
		return vec[index];
	}

	template<unsigned index>
	void Set(float val)
	{
		vec[index] = val;
	}

	T GetVec()
	{
		return vec;
	}

private:
	T vec;
};

