#pragma once
#include <SDL.h>

class WindowHandler
{
private:

public:
	WindowHandler();
	~WindowHandler();

	static WindowHandler &GetInstance();
	void Destroy();

	SDL_Window* gameWindow;
	SDL_GLContext renderContext;
	SDL_GLContext loadingContext;
};

