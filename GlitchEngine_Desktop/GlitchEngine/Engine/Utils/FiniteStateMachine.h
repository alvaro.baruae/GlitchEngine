#pragma once
#include <vector>
#include <stdio.h>
#include <map>

#include <Utils/LuaEventDispatcher.h>

using namespace std;

enum FSMOperations
{
	EQ = 1 << 0,
	NEQ = 1 << 1,
	L = 1 << 2,
	LEQ = 1 << 3,
	G = 1 << 4,
	GEQ = 1 << 5,
};

struct FSMTransition
{
	FSMOperations operation;
	float* value1;
	float* value2;
	string toState;
};

struct FSMState {
	string key;
	vector<FSMTransition> transitions;
};

class FiniteStateMachine
{
private:

	LuaEventDispatcher* LED;
	int callbackIndex;
	string currentState;
	map<string, float> parameters;
	map<string, FSMState> states;
	bool CheckTransition(FSMTransition trans);
	void UpdateState();

public:
	FiniteStateMachine();
	~FiniteStateMachine();

	string GetCurrentState() const;
	void AddState(string name);
	void SetStartState(string name);
	void SetParameter(string name, float value);

	void AddTransition(string stateName, string destinationStateName, unsigned int operation, string param1, string param2);
	void RegisterCallback();
};