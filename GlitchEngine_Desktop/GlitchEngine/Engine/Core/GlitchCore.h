#pragma once

#define GLEW_STATIC

#include <glew.h>
#include <SDL.h>
#include <AL/alc.h>
#include <AL/alext.h>
#include <glm/glm.hpp>
#include <Box2D/Box2D.h>

#include <vector>
#include <map>
#include <stdio.h>
#include <chrono>

#include <Classes/SpriteAnimator.h>
#include <Classes/PhysicsWorld.h>
#include <Classes/LightScreen.h>

#include <Objects/GameObject.h>
#include <Objects/SoundManager.h>
#include <Objects/Camera.h>
#include <Objects/FrameBuffer.h>

#include <Utils/Logger.h>
#include <Utils/WindowHandler.h>
#include <Utils/Application.h>
#include <Utils/JSonParser.h>
#include <Utils/LuaInterpreter.h>
#include <Utils/TimeHandle.h>
#include <Utils/Input.h>
#include <Utils/ResourceManager.h>
#include <Utils/ContactListener.h>
#include <Utils/PhysicsDebugDraw.h>
#include <Utils/TiledMapParser.h>
#include <Utils/MovieQuadManager.h>

typedef std::chrono::high_resolution_clock Clock;

class GlitchCore
{
public:
	GlitchCore();
	~GlitchCore();
	bool Initialize();
	void Run();
	void Terminate();
	void TimedLoop();

private:

	void GetDelta();
	ALCdevice* oalDevice;
	ALCcontext* oalContext;

	Camera* camera = nullptr;
	JSonParser* config = nullptr;
	LuaInterpreter *lua = nullptr;
	FrameBuffer* fbo = nullptr;
	Input* input = nullptr;
	SpriteAnimator* spriteAnimator = nullptr;
	LightScreen* lightScreen = nullptr;
	TiledMapParser* tiledParser = nullptr;
	ContactListener* listener = nullptr;
	PhysicsDebugDraw* debugDraw = nullptr;
	b2ContactFilter* filtering = nullptr;
	WindowHandler* windowHandler = nullptr;
	ApplicationData* appData = nullptr;
	TimeHandle* timeHandle = nullptr;
	PhysicsWorld* pWorld = nullptr;
	ResourceManager* RM = nullptr;

	std::chrono::high_resolution_clock::time_point lastTime;
	long long counter = 0;
	bool quit = false;
	bool vSync = false;
};

