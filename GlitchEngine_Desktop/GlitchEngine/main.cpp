#include <Core/GlitchCore.h>

int main(int argc, char *argv[])
{
	GlitchCore core;
	if (core.Initialize())
		core.Run();
	core.Terminate();
	return 0;
}