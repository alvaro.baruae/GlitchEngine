Enemy = {
	mt = {},
	gameObject = nil,

	Instantiate = function(position)
		local prefab = {}
		prefab.gameObject = Sprite(position, 0, Vec2(24, 24), 1, false, "Characters")
		AddComponent(prefab.gameObject, "EnemyController")
		setmetatable(prefab, Enemy.mt)
		return prefab
	end
}

Enemy.mt.__metatable = "Private"

Enemy.mt.__index = function(table, key)
	print("Key Not Found")
end

Enemy.mt.__newindex = function(table, key, value)
	print("Prefabs Are Private")
end 

-- Destructor
Enemy.mt.__gc = function(self)
	Destroy(self.gameObject)
end