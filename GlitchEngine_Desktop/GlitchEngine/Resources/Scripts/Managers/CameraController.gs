CameraController = {
	playerTransform = nil,
	camTransform = nil,
	offset = nil,
	playerController = nil,
	dampening = nil,
	shakeIntensity = 0,
	shaking = false,

	Init = function(player)
		playerController = GetComponent(player, "PlayerController")
		playerTransform = player:GetTransform()
		camTransform = Camera.GetMainCamera():GetTransform()
		Camera.GetMainCamera().size = 3.15
		offset = Vec2(-80, 53)
		dampening = 5
	end,

	Update = function()
		if playerTransform ~= nil then
			local newPos
			if playerController:GetLookingRight() then
				newPos = playerTransform.position + Vec2(-offset.x, offset.y)
			else
				newPos = playerTransform.position + offset
			end

			if shaking then
				newPos = newPos + RandomUnitCircle():ScalarMult(shakeIntensity * 10)
				shakeIntensity = shakeIntensity - 0.1
				if shakeIntensity <= 0 then
					shaking = false
				end
			end

			camTransform.position = Vec2Lerp(camTransform.position, newPos, Time.deltaTime * dampening)
		end
	end,

	FireShake = function()
		shakeIntensity = 1
		shaking = true
	end
}