local moveInTSGUI = false
local moveOutTSGUI = false
local showTSGUI = false
local startTime = 0
local shaderIndex = 0
local btnStartGamePostition = Vec2(420, -80)
local guiHeightModifier = Vec2(0, 550)
local textPanelMod = Vec2(0, 253)
local showFirstSceneGUI = false
local playerController = nil
local gamePlayGUI = false
local showEButton = false
local showTextPanel = false
local hideTextPanel = false
local panelIsMoving = false
local panelStart = Vec2(0,253)
local panelSound = nil
local showEndScreen = false

local shaders = {}

shaders[0] = {}
shaders[0][0] = "Normal"
shaders[0][1] = Vec2(530, 555)

shaders[1] = {}
shaders[1][0] = "Sepia"
shaders[1][1] = Vec2(540, 555)

shaders[2] = {}
shaders[2][0] = "Negative"
shaders[2][1] = Vec2(510, 555)

shaders[3] = {}
shaders[3][0] = "Grey Scale"
shaders[3][1] = Vec2(495, 555)

shaders[4] = {}
shaders[4][0] = "Chrom. Aberration"
shaders[4][1] = Vec2(423, 555)

GUIController = {
	
	Init = function(pController)
		playerController = pController
		panelSound = SoundSource("Panel")
	end,

	OnGUI = function()

		if not showEndScreen then
			if moveInTSGUI == true then
				local deltaPosition = (Time.time - startTime) / 0.5
				btnStartGamePostition = Vec2OutQuadEasing(Vec2(420, -80), Vec2(420, 135), deltaPosition)
				guiHeightModifier = Vec2OutQuadEasing(Vec2(0, 550), Vec2(0, 0), deltaPosition)
				if deltaPosition >= 1 then
					moveInTSGUI = false
					btnStartGamePostition = Vec2(420, 135)
					guiHeightModifier = Vec2(0,0)
				end
			end

			if moveOutTSGUI == true then
				local deltaPosition = (Time.time - startTime) / 0.5
				btnStartGamePostition = Vec2OutQuadEasing(Vec2(420, 135),Vec2(420, -80), deltaPosition)
				guiHeightModifier = Vec2OutQuadEasing(Vec2(0, 0), Vec2(0, 550), deltaPosition)
				if deltaPosition >= 1 then
					showTSGUI = false
					moveOutTSGUI = false
					btnStartGamePostition = Vec2(420, -80)
					guiHeightModifier = Vec2(0, 550)
				end
			end

			if showTSGUI == true then
				GUI:Button(btnStartGamePostition, Vec2(320,80), "btn_StartGame", GameManager.StartGame)
				GUI:Label("Choose a Filter", Vec2(445, 513) + guiHeightModifier, "VCR",  24)
				GUI:Label(shaders[shaderIndex][0], shaders[shaderIndex][1] + guiHeightModifier, "VCR",  24)
				GUI:Button(Vec2(362,555) + guiHeightModifier, Vec2(48,41), "btn_LeftArrow", GUIController.ClickLeftArrow)
				GUI:Button(Vec2(795,555) + guiHeightModifier, Vec2(48,41), "btn_RightArrow", GUIController.ClickRightArrow)
			end

			if showFirstSceneGUI == true then
				GUI:Image(Vec2(34, 489), Vec2(406,145), "Keys_Tut")
			end

			if gamePlayGUI then
				for i = 0, playerController.GetLives() - 1 do
					GUI:Image(Vec2(18 + 55 * i,1), Vec2(45,27), "Crown")
				end
				GUI:Image(Vec2(0,0), Vec2(960,30), "Blackbar")
				
				if showEButton then
					GUI:Image(Vec2(885,25), Vec2(75,75), "E")
				end

				if showTextPanel then
					local deltaPosition = (Time.time - startTime) / 0.5

					if hideTextPanel then
						textPanelMod = Vec2OutQuadEasing(panelStart, Vec2(0,0), deltaPosition)
					else
						textPanelMod = Vec2OutQuadEasing(Vec2(0,0), Vec2(0, 253), deltaPosition)
					end

					if deltaPosition >= 1 then
						panelIsMoving = false
						if hideTextPanel then
							textPanelMod = Vec2(0,0)
							panelStart = Vec2(0,253)
							showTextPanel = false
							hideTextPanel = false
						else
							textPanelMod = Vec2(0,253)
						end
					end

					GUI:Image(Vec2(185,-193) + textPanelMod, Vec2(596,193), "TxtPanel")
				end
			end
		else
			GUI:Image(Vec2(0,0), Vec2(960,640), "Finish")
		end
	end,

	ClickLeftArrow = function()
		click:Play(false)
		if shaderIndex - 1 < 0 then
			shaderIndex = 5
		end
		shaderIndex = shaderIndex - 1
		GameManager.SetShader(shaderIndex)
	end,

	ClickRightArrow = function()
		click:Play(false)
		if shaderIndex + 1 > 4 then
			shaderIndex = -1
		end
		shaderIndex = shaderIndex + 1
		GameManager.SetShader(shaderIndex)
	end,

	ShowTSGUI = function()
		showTSGUI = true
		moveInTSGUI = true
		startTime = Time.time
	end,

	HideTSGUI = function()
		startTime = Time.time
		moveOutTSGUI = true
	end,

	ShowFirstSceneGUI = function()
		showFirstSceneGUI = true
		gamePlayGUI = true
	end,

	ShowEButton = function()
		showEButton = true
	end,

	HideEButton = function()
		showEButton = false
	end,

	ShowTextPanel = function()
		if showTextPanel == false and panelIsMoving == false then
			panelSound:SetPitch(1)
			panelSound:Play(false)
			startTime = Time.time
			showTextPanel = true
			panelIsMoving = true
		elseif hideTextPanel == false and panelIsMoving == false then
			panelSound:SetPitch(0.5)
			panelSound:Play(false)
			startTime = Time.time
			hideTextPanel = true
			panelIsMoving = true
		end
	end,

	HideTextPanel = function()
		if showTextPanel and hideTextPanel == false then
			panelSound:SetPitch(0.5)
			panelSound:Play(false)
			panelStart = textPanelMod
			startTime = Time.time
			hideTextPanel = true
		end
	end,

	HideTut = function()
		showFirstSceneGUI = false
	end,

	ShowEndScreen = function()
		showEndScreen = true
	end
}