local shake = false
local topTile
local botTile
local shakeIntensity = 10
local pathBlockAnim = false
local startTime = 0
local pController
local translate = false
local startX = 0
local destinationX = 0

CameraController = {
	camera = nil,
	transform = nil,

	Awake = function()
		camera = Camera.GetMainCamera()
		transform = camera:GetTransform()
	end,

	Update = function()
		if shake == true then
			transform.position = Vec2(math.random(), math.random()):ScalarMult(shakeIntensity)
			shakeIntensity = shakeIntensity - 0.05
			if shakeIntensity <= 0 then
				transform.position = Vec2(0,0)
				shake = false
			end
		end

		if pathBlockAnim == true then
			local deltaAnim = (Time.time - startTime) / 2.5
			local hDiff = Lerp(0,128, deltaAnim)
			topTile.quad:GetTransform().position = Vec2(-448, -160 + hDiff)
			botTile.quad:GetTransform().position = Vec2(-448, -224 + hDiff)
			if deltaAnim >= 1 then
				pathBlockAnim = false
				topTile.quad:GetTransform().position = Vec2(-448, -32)
				botTile.quad:GetTransform().position = Vec2(-448, -96)
				pController.LookRight()
				pController.GainControl()
			end
		end

		if translate then
			local deltaAnim = (Time.time - startTime) / 1.5
			local newX = Lerp(startX,destinationX, deltaAnim)
			transform.position = Vec2(newX, transform.position.y)
			if deltaAnim >= 1 then
				translate = false
				transform.position = Vec2(destinationX, transform.position.y)
			end
		end
	end,

	CameraShake = function(playerController)
		pController = playerController
		shake = true
		pathBlockAnim = true
		startTime = Time.time
		topTile = LevelLoader.SpawnTile(6, Vec2(-448, -160), 0)
		botTile = LevelLoader.SpawnTile(16, Vec2(-448, -224), 0)
	end,

	CameraTranslateRight = function()
		translate = true
		startTime = Time.time
		startX = 0
		destinationX = 960
	end,

	CameraTranslateLeft = function()
		translate = true
		startTime = Time.time
		destinationX = 0
		startX = 960
	end
}