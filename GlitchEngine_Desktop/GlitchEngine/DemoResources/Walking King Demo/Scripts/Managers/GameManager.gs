function Lerp(a, b, t)
	return a + (b - a) * t
end

function Vec2Lerp(a, b, t)
	local x = a.x + (b.x - a.x) * t
	local y = a.y + (b.y - a.y) * t 
	return Vec2(x,y)
end

function Vec2OutQuadEasing(a, b, t)
	local x = -(b.x-a.x) * t * (t - 2) + a.x
	local y = -(b.y-a.y) * t * (t - 2) + a.y
	return Vec2(x,y)
end


local moveCamera = false
local canFadeIn = false
local canFadeOut = false
local cutOffValue = 0
local startTime = 0
local levelIndex = 0
local playerDied = false
local endGame = false

GameManager = {
	player = nil,
	bgm = nil,
	click = nil,
	load = nil,

	Awake = function()
		SoundListener.SetGain(0.5)
		Resources:LoadTexture("AlphaBlend","Images/AlphaBlend.png", LINEAR_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("AlphaBlend2","Images/AlphaBlend2.png", LINEAR_FILTER, CLAMP_WRAP)

		PhysicsWorld:SetSimulationUnitConversion(16)
		PhysicsWorld:SetGravity(Vec2(0, -9.81))
		PhysicsWorld.debugDraw = false

		PostProcessing.includeGUI = true
		PostProcessing:SetShader("pp_fade")
		PostProcessing:SetTexture("other", "AlphaBlend")
		PostProcessing:SetFloat("cutOff", cutOffValue)
		PostProcessing.enabled = true
	end,

	Init = function(self)
		player = Player.Instantiate(Vec2(2030, -84))
		GUIController.Init(GetComponent(player.gameObject, "PlayerController"))
		
		click = SoundSource("Click")
		load = SoundSource("Load")
		
		GameManager.LoadNextLevel()
	end,

	Update = function()
		if canFadeIn == true then
			local deltaFade = Time.time - startTime
			cutOffValue = Lerp(0,1, deltaFade)
			bgm:SetVolume(cutOffValue)
			PostProcessing:SetFloat("cutOff", cutOffValue)
			if deltaFade >= 1 then
				canFadeIn = false
				cutOffValue = 1
				PostProcessing:SetFloat("cutOff", cutOffValue)
			end
		end

		if canFadeOut == true then
			local deltaFade = Time.time - startTime
			cutOffValue = Lerp(1,0, deltaFade)
			bgm:SetVolume(cutOffValue)
			PostProcessing:SetFloat("cutOff", cutOffValue)
			if deltaFade >= 1 then
				canFadeOut = false
				if not playerDied then
					Camera.GetMainCamera():GetTransform().position = Vec2(0,0)
					LevelLoader.DeleteLastLoadedLevel()
					GameManager.LoadNextLevel()
					cutOffValue = 0
					PostProcessing:SetFloat("cutOff", cutOffValue)
					PostProcessing:SetTexture("other", "AlphaBlend2")
					GUIController.ShowFirstSceneGUI()
				else
					if endGame then
						GUIController.ShowEndScreen()
					else
						GetComponent(player.gameObject, "PlayerController").ResetPlayer()
					end
					
					canFadeIn = true
					startTime = Time.time
				end
			end
		end

		if moveCamera == true then
			local deltaCamera = (Time.time - startTime) / 4
			Camera.GetMainCamera():GetTransform().position = Vec2Lerp(Vec2(0,0), Vec2(1920, 0), deltaCamera)
			if deltaCamera >= 1 then
				Camera.GetMainCamera():GetTransform().position = Vec2(1920, 0)
				moveCamera = false
				GUIController.ShowTSGUI()
			end
		end
	end,

	DoneLoadingScene = function(sceneName)

		if sceneName == "TittleScreen" then
			startTime = Time.time
			canFadeIn = true
			moveCamera = true
			bgm = SoundSource("BGM1")
			bgm:SetVolume(0)
			bgm:Play(true)
		end

		if sceneName == "IntroLevel" then
			LevelLoader.SpawnLevelCollision("Collision", Vec2(0, 0))
			startTime = Time.time
			canFadeIn = true
			Camera.GetMainCamera().size = 1.02
			bgm = SoundSource("BGM2")
			bgm:Play(true)
			player.gameObject:GetTransform().position = Vec2(-530, -84)
			GetComponent(player.gameObject, "PlayerCinematicController").PlayIntroAnimation()
		end

		if sceneName == "Level2" then
			LevelLoader.SpawnLevelCollision("Collision", Vec2(960, 0))
		end
	end,

	PlayerOutOfFrameIntro = function()
		canFadeOut = true
		startTime = Time.time
	end,

	StartGame = function()
		load:Play(false)
		GetComponent(player.gameObject, "PlayerCinematicController").PlayTittleScreenAnim()
		GUIController.HideTSGUI()
	end,

	SetShader = function(shaderIndex)

		if shaderIndex == 0 then
			PostProcessing:SetShader("pp_fade")
		elseif shaderIndex == 1 then
			PostProcessing:SetShader("pp_sepia")
		elseif shaderIndex == 2 then
			PostProcessing:SetShader("pp_ng")
		elseif shaderIndex == 3 then
			PostProcessing:SetShader("pp_gs")
		elseif shaderIndex == 4 then
			PostProcessing:SetShader("pp_ca")
		end

		PostProcessing:SetFloat("cutOff", cutOffValue)
	end,

	LoadNextLevel = function()
		local layers = {}

		if levelIndex == 0 then
			layers[0] = "Background"
			layers[1] = "Floor"
			layers[4] = "Foreground"
			LevelLoader.RenderScene("TittleScreen", layers, Vec2(0,0))
			levelIndex = levelIndex + 1
			return
		end

		if levelIndex == 1 then
			layers[0] = "Background"
			layers[1] = "Floor"
			layers[4] = "Foreground"
			LevelLoader.RenderScene("IntroLevel", layers, Vec2(0,0))
			levelIndex = levelIndex + 1
			return
		end

		if levelIndex == 2 then
			layers[0] = "Water"
			layers[1] = "Background"
			layers[2] = "Floor"
			LevelLoader.RenderScene("Level2", layers, Vec2(960,0))
			levelIndex = levelIndex + 1
			return
		end
	end,

	ResetLevel = function()
		startTime = Time.time
		canFadeOut = true
		playerDied = true
	end,

	EndGame = function()
		startTime = Time.time
		canFadeOut = true
		playerDied = true
		endGame = true
	end
}