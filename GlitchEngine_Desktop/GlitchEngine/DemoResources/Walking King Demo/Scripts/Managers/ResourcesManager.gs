ResourcesManager = {
	Awake = function()

		--Fonts
		Resources:LoadFont("VCR","Fonts/VCR.ttf")

		--Animation
		Resources:LoadAnimation("King","Animations/King.json")

		--Textures
		Resources:LoadTexture("King","Animations/King.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("Env_Tiles","Maps/Env_Tiles.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("btn_StartGame","GUI/btn_StartGame.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("btn_LeftArrow","GUI/btn_LeftArrow.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("btn_RightArrow","GUI/btn_RightArrow.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("Keys_Tut","GUI/Keys_Tut.png", LINEAR_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("Blackbar","GUI/blackBar.png", POINT_FILTER, REPEAT_WRAP)
		Resources:LoadTexture("Crown","GUI/Lives.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("E","GUI/btn_E.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("TxtPanel","GUI/txtPanel.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadTexture("Finish","GUI/EndScreen.png", POINT_FILTER, CLAMP_WRAP)

		--Music
		Resources:LoadSound("BGM1","Music/BGM1.ogg")
		Resources:LoadSound("BGM2","Music/BGM2.ogg")

		-- SFX
		Resources:LoadSound("Click","SFX/Click.wav")
		Resources:LoadSound("Load","SFX/Load.wav")
		Resources:LoadSound("sfx_Rock","SFX/RockBreak.wav")
		Resources:LoadSound("Die","SFX/Die.wav")
		Resources:LoadSound("Panel","SFX/PanelSFX.wav")
		Resources:LoadSound("Jump","SFX/Jump.wav")
		Resources:LoadSound("JumpHit","SFX/JumpHit.wav")
		Resources:LoadSound("Win","SFX/Win.wav")
		GameManager:Init()
	end
}