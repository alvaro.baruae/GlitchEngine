Player = {
	mt = {},
	mat = nil,
	gameObject = nil,

	Instantiate = function(position)
		local prefab = {}

		prefab.mat = Material("GE_Sprited", false)
		prefab.mat:SetTexture("texture","King")
		prefab.gameObject = Quad(position,0,Vec2(64,64),3,prefab.mat)
		AddComponent(prefab.gameObject,  "PlayerController")
		AddComponent(prefab.gameObject,  "PlayerCinematicController")
		setmetatable(prefab, Player.mt)
		return prefab
	end
}

Player.mt.__metatable = "Private"

Player.mt.__index = function(table, key)
	print("Key Not Found")
end

Player.mt.__newindex = function(table, key, value)
	print("Prefabs Are Private")
end 

-- Destructor
Player.mt.__gc = function(self)
	Destroy(self.gameObject)
	Destroy(self.mat)
end