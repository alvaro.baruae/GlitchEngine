#version 330

in vec3 vertexPosition; 
in vec2 textureCoords; 
uniform mat4 MVP; 
out vec2 UV; 

void main()
{ 
	UV = textureCoords; 
	gl_Position = MVP * vec4(vertexPosition.xy, 0.0, 1.0); 
}