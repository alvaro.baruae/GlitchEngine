#version 330
in vec2 UV; 
uniform sampler2D texture; 
uniform sampler2D other;
uniform float cutOff;

out vec4 fragColor;

void main() 
{ 
	vec4 texture = texture2D(texture, UV);
	float alphaBlend = texture2D(other, UV).r;
    if(alphaBlend <= cutOff)
    {
		float accum = (texture.r + texture.g + texture.b) / 3;
		fragColor = vec4(vec3(accum) , 1.0f);
	}
	else
		fragColor = vec4(0,0,0,1);
}
