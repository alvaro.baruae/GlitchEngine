#version 330
in vec2 UV; 
uniform sampler2D texture; 
uniform sampler2D other;
uniform float cutOff;
out vec4 fragColor; 

void main () {

	vec3 color = texture2D(texture, UV).rgb;
	float alphaBlend = texture2D(other, UV).r;
    if(alphaBlend <= cutOff)
    	fragColor = vec4(vec3(1.0 - color), 1.0);
	else
		fragColor = vec4(0,0,0,1);
}