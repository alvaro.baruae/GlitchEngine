ResourceManager = {

	Awake = function()
		Resources:LoadTexture("Characters","Animations/Characters.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadAnimation("Characters","Animations/Characters.json")
		Resources:LoadTexture("Bullet","Animations/Bullet.png", POINT_FILTER, CLAMP_WRAP)
		Resources:LoadAnimation("Bullet","Animations/Bullet.json")
		Resources:LoadTexture("Tile","Map/WallTile.jpg", POINT_FILTER, CLAMP_WRAP)

		GameManager:Init()
	end
}