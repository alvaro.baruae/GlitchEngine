local player = nil
local enemies = {}
local canSpawn = false
local counter = 0

GameManager = {

	Init = function()
		PhysicsWorld:SetSimulationUnitConversion(24)
		PhysicsWorld:SetGravity(Vec2(0, -20))

		LevelLoader.RenderScene("Testbed", {"Floor"}, Vec2(-480/2, 160/2) , GameManager.DoneLoadingScene)
		LevelLoader.SpawnCollision("Collision", Vec2(-480/2, 160/2))
	end,

	Update = function()
		if canSpawn then
			counter = counter + Time.deltaTime
			if counter > spawnInterval then
				GameManager:SpawnEnemy()
				counter = 0
			end
		end
	end,

	DoneLoadingScene = function()
		player = Player.Instantiate(Vec2(208, -50))

		if enemiesSpawn == true then 
			GameManager:SpawnEnemy()
			canSpawn = true
		end

		if camera == true then
			CameraController.Init(player.gameObject)
		end
	end,

	SpawnEnemy = function()
		table.insert(enemies, Enemy.Instantiate(Vec2(208, 50)))
	end
}