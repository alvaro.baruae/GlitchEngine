Player = {
	mt = {},
	gameObject = nil,

	Instantiate = function(position)
		local prefab = {}
		prefab.gameObject = Sprite(position, 0, Vec2(24,24), 2, false, "Characters")
		AddComponent(prefab.gameObject,  "PlayerController")
		setmetatable(prefab, Player.mt)
		return prefab
	end
}

Player.mt.__metatable = "Private"

Player.mt.__index = function(table, key)
	print("Key Not Found")
end

Player.mt.__newindex = function(table, key, value)
	print("Prefabs Are Private")
end 

-- Destructor
Player.mt.__gc = function(self)
	Destroy(self.gameObject)
end